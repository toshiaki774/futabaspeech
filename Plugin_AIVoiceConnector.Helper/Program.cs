﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using AI.Talk.Editor.Api;

namespace Plugin_AIVoiceConnector.Helper
{
    public class MasterControl
    {
        public double Volume { get; set; } = 1.0;
        public double Speed { get; set; } = 1.0;
        public double Pitch { get; set; } = 1.0;
        public double PitchRange { get; set; } = 1.0;
    }
    public class SpeechTask
    {
        private TtsControl _tts;
        private NamedPipeServerStream _pipe;
        public int Status { get; set; } = 0;
        public SpeechTask(TtsControl tts, NamedPipeServerStream pipe)
        {
            _tts = tts;
            _pipe = pipe;
        }
        public async Task Run(string text, JObject speechParams)
        {
            await Task.Run(() =>
            {
                try
                {
                    while (_tts.Status == HostStatus.Busy)
                    {
                        Thread.Sleep(50);
                    }
                    if (_tts.Status != HostStatus.Idle)
                    {
                        Status = -1;
                        return;
                    }
                    var origParams = _tts.MasterControl;
                    var origPreset = _tts.CurrentVoicePresetName;
                    MasterControl newParams = null;
                    string newPreset = origPreset;
                    try
                    {
                        newParams = JsonConvert.DeserializeObject<MasterControl>(_tts.MasterControl);
                    }
                    catch (Exception)
                    {
                    }
                    if (newParams == null) newParams = new MasterControl();
                    if (speechParams != null)
                    {
                        var volume = speechParams.SelectToken("volume");
                        var speed = speechParams.SelectToken("speed");
                        var pitch = speechParams.SelectToken("pitch");
                        var preset = speechParams.SelectToken("preset");
                        var presetIdx = speechParams.SelectToken("presetIndex");
                        if (volume != null) newParams.Volume = volume.Value<double>() / 100.0;
                        if (speed != null) newParams.Speed = speed.Value<double>() / 100.0;
                        if (pitch != null) newParams.Pitch = pitch.Value<double>() / 100.0;
                        if (preset != null)
                        {
                            foreach (var elem in _tts.VoicePresetNames)
                            {
                                if (elem == preset.Value<string>()) newPreset = elem;
                            }
                        }
                        else if (presetIdx != null)
                        {
                            var presets = _tts.VoicePresetNames;
                            var idx = presetIdx.Value<int>() - 1;
                            if (idx >= 0 && idx < presets.Length) newPreset = presets[idx];
                        }
                    }
                    _tts.CurrentVoicePresetName = newPreset;
                    _tts.MasterControl = JsonConvert.SerializeObject(newParams);
                    _tts.Text = text;
                    _tts.Play();
                    do
                    {
                        Thread.Sleep(50);
                    } while (_tts.Status == HostStatus.Busy);
                    _tts.CurrentVoicePresetName = origPreset;
                    _tts.MasterControl = origParams;
                    Status = 1;
                }
                catch (Exception)
                {
                    Status = -1;
                }
            });
        }
        public void Cancel()
        {
            if (Status != 0) return;
            _tts.Stop();
        }
    }
    class Program
    {
        static TtsControl _ttsControl;
        static void Main(string[] args)
        {
            Console.WriteLine("このプログラムは棒読みちゃんのフォルダに置くことでPlugin_AIVoiceConnectorにより自動的に起動されます。");
            InitializeTts();

            var pipeName = "AIVoiceConnector";
            if (args.Length > 0)
            {
                pipeName += $"-{args[0]}";
            }

            _ = RunServerAsync(pipeName);
            Console.WriteLine("リターンキーを入力すると終了します。");
            Console.ReadLine();
        }
        static void InitializeTts()
        {
            if (_ttsControl != null) return;
            var tts = new TtsControl();
            var ttsHosts = tts.GetAvailableHostNames();
            if (ttsHosts.Length > 0)
            {
                tts.Initialize(ttsHosts[0]);
                if (tts.Status == HostStatus.NotRunning)
                {
                    tts.StartHost();
                }
                if (tts.Status == HostStatus.NotConnected)
                {
                    tts.Connect();
                }
                _ttsControl = tts;
            }
        }
        public static Task RunServerAsync(string pipeName)
        {
            return Task.Run(async () =>
            {
                bool quit = false;
                while (!quit)
                {
                    try
                    {
                        using (var pipeServer = new NamedPipeServerStream(pipeName, PipeDirection.InOut, 1, PipeTransmissionMode.Byte, PipeOptions.Asynchronous))
                        {
                            var canceller = new CancellationTokenSource();
                            await pipeServer.WaitForConnectionAsync();
                            using (var reader = new StreamReader(pipeServer))
                            {
                                Task currentTask = null;
                                SpeechTask task = null;
                                while (true)
                                {
                                    string statusMsg = "unknown";
                                    string recvString = await reader.ReadLineAsync();
                                    if (recvString == null) break;
                                    JObject json = null;
                                    try
                                    {
                                        json = JObject.Parse(recvString);
                                    }
                                    catch (JsonReaderException ex)
                                    {
                                        SendResponse("json parse error", pipeServer);
                                        break;
                                    }
                                    var command = json.SelectToken("command");
                                    if (command != null)
                                    {
                                        InitializeTts();
                                        if (_ttsControl.Status == HostStatus.NotConnected)
                                        {
                                            _ttsControl.Connect();
                                        }
                                        switch (command.Value<string>())
                                        {
                                            case "play":
                                                var text = json.SelectToken("text");
                                                var speechParams = json.SelectToken("params");
                                                if (text == null)
                                                {
                                                    statusMsg = "empty text";
                                                    break;
                                                }
                                                task = new SpeechTask(_ttsControl, pipeServer);
                                                currentTask = task.Run(text.Value<string>(), speechParams?.Value<JObject>());
                                                statusMsg = "ok";
                                                break;
                                            case "getStatus":
                                                if (task != null)
                                                {
                                                    if (task.Status == 0) statusMsg = "busy";
                                                    else if (task.Status > 0) statusMsg = "done";
                                                    else statusMsg = "error";
                                                }
                                                else
                                                {
                                                    statusMsg = "idle";
                                                }
                                                break;
                                            case "skip":
                                                if (task != null) task.Cancel();
                                                statusMsg = "ok";
                                                break;
                                            case "quit":
                                                quit = true;
                                                statusMsg = "ok";
                                                break;
                                        }
                                    }
                                    SendResponse(statusMsg, pipeServer);
                                }
                                if (currentTask != null)
                                {
                                    await currentTask;
                                    currentTask = null;
                                }
                            }
                        }
                    }
                    catch (IOException ex)
                    {
                    }
                }
            });
        }
        static void SendResponse(string msg, NamedPipeServerStream pipe)
        {
            var writer = new StreamWriter(pipe);
            writer.WriteLine($"{{\"status\":{JsonConvert.ToString(msg)}}}");
            writer.Flush();
        }
    }
}
