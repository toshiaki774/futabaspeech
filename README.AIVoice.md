プログラムのダウンロードは[こちら](https://gitlab.com/toshiaki774/futabaspeech/-/releases)

FutabaSpeech同梱のPlugin_AIVoiceConnectorについて
=====================================================
A.I.Voiceがインストールされている環境において、棒読みちゃんにA.I.Voice経由で喋らせるためのプラグインです。
VoiceroidやA.I.Voice2には対応していません。

[VoiceroidTalkPlus](https://wangdora.jp/?plugin/VTP)や[PotatoVoiceHub](https://github.com/gizagizamax/PotatoVoiceHub)のプラグインと同時に使うことはできません。

インストール方法
================
 0. `Plugin_VoiceroidTalkPlus`や`Plugin_PotatoVoiceHub`が棒読みちゃんにインストールされている場合、無効化もしくは取り除く
 1. `Plugin_AIVoiceConnector.dll`と`Plugin_AIVoiceConnector.Helper.exe`を棒読みちゃんのフォルダにコピーする
 2. 棒読みちゃんを起動し、「その他」タブでAIVoiceConnectorプラグインが有効化されていることを確認する
 3. A.I.VOICE Editorプログラムを (起動していなければ) 起動する
 4. 棒読みちゃんに喋らせる

何らかの理由でA.I.Voiceに接続できない場合は、棒読みちゃんの内蔵ボイス (女性1) で喋ります。

FutabaSpeechやHTTP/Socket経由での使い方
=======================================
デフォルト (声質を指定しない場合) では、A.I.Voice側で現在選択中のボイスプリセットを利用して喋ります。
FutabaSpeechの声質で「その他」を選択し、ボイス番号を20001～20020の範囲で指定すると、
A.I.Voiceに登録されているプリセットの1～20番目のものを選択して喋ります。
プリセットには標準ボイスとユーザーボイスがありますが、ユーザーボイスの方が先に並ぶようです。
例えば標準ボイスが2つ、ユーザーボイスが2つ登録されている場合、

 - 20001 → ユーザーボイスの1番目
 - 20002 → ユーザーボイスの2番目
 - 20003 → 標準ボイスの1番目
 - 20004 → 標準ボイスの2番目
 
という対応付けが行われるようです。このあたりは環境による可能性もあるので、各自で確認をお願いします。

HTTPやSocketを使って他の外部アプリから棒読みちゃんに喋らせる場合も同様です。

制限
====
`教育(YouTube=ユーチューブ)`等、棒読みちゃんのタグ機能を使った文章を読み上げる場合、
A.I.Voiceではなく棒読みちゃんの内蔵ボイスで喋ります。
