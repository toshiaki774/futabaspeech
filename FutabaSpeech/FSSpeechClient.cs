﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace FutabaSpeech
{
    public static class FSSpeechClient
    {
        private static Int64 _lastSpeechTicks;
        private static LinkedList<FSMessage> _messageQueue = new LinkedList<FSMessage>();
        private static LinkedList<FSMessage> _displayMessageQueue = new LinkedList<FSMessage>();
        private static HashSet<int> _messageIds = new HashSet<int>();
        private static int _currentTaskFirstId;
        private static int _currentTaskLastId;
        public static bool Running;
        private static bool _quit;
        private static bool _skip;

        public static async Task Run()
        {
            Running = true;
            _quit = false;
            await Task.Run(() =>
            {
                bool skipPlayingCheck = false;
                bool waiting = false;
                int pausingTaskId = -1;
                Int64 lastUnreadCountAlertTicks = 0;
                Int64 timeoutCountdownStartedTicks = 0;
                while (Running && (!_quit || _messageQueue.Count > 0))
                {
                    double interval = FSSettings.SharedInstance().SpeechInterval;
                    Int64 now = DateTime.Now.Ticks;
                    
                    if (_skip)
                    {
                        _skip = false;
                        if (interval > 0)
                        {
                            CancelPlaying();
                            if (_messageQueue.Count > 0)
                            {
                                waiting = false;
                                now = (long)(_lastSpeechTicks + interval * TimeSpan.TicksPerMillisecond + 1000);
                            }
                        }
                    }
                    if (FSSettings.SharedInstance().ContextProps.ShouldAlertUnreadMessageCount && HasElapsed(60000, lastUnreadCountAlertTicks))
                    {
                        if (_messageQueue.Count >= Math.Max(1,FSSettings.SharedInstance().ContextProps.UnreaAlertThreshold))
                        {
                            lastUnreadCountAlertTicks = DateTime.Now.Ticks;
                            SpeechText(FSSettings.SharedInstance().GetUnreadAlertMessage(_messageQueue.Count), 0, false);
                        }
                    }
                    if (_messageQueue.Count != 0 && !waiting && HasElapsed(interval, _lastSpeechTicks, now))
                    {
                        FSMessage message = null;
                        lock (_messageQueue)
                        {
                            var first = _messageQueue.First;
                            if (first != null)
                            {
                                message = _messageQueue.First.Value;
                                _messageQueue.RemoveFirst();
                                _messageIds.Remove(message.MessageId);
                            }
                        }
                        if (message != null)
                        {
                            int taskId = 0;
                            foreach (var textGroup in message.LineGroups)
                            {
                                int ret = SpeechText(textGroup.Item1.ToString(), message.MessageId, textGroup.Item2);
                                if (taskId == 0) taskId = ret;
                                _currentTaskLastId = ret;
                            }
                            message.TaskId = taskId;
                            _currentTaskFirstId = taskId;
                            if (message.MessageId != 0) _displayMessageQueue.AddLast(message);
                            if (taskId > 0 && interval != 0)
                            {
                                waiting = true;
                                /* task should start at least within about 1 sec if BouyomiChan is idle
                                 * otherwise timeout occurs and the next task will be started (for tasks whose last message is silent)
                                 * this was initially set to 500 ms, but when using SAPI voices
                                 * occasionally it takes more than 500 ms - so increased to 1 sec */
                                skipPlayingCheck = true;
                                pausingTaskId = -1;
                                timeoutCountdownStartedTicks = DateTime.Now.Ticks;
                            }
                            UpdateStatus();
                        }
                    }
                    int playingTaskId = -1;
                retry:
                    if (_displayMessageQueue.Count > 0)
                    {
                        if (playingTaskId < 0)
                        {
                            playingTaskId = GetCurrentTaskId();
                            if (playingTaskId == _currentTaskLastId) skipPlayingCheck = false;
                        }
                        do
                        {
                            var message = _displayMessageQueue.First.Value;
                            if (message.TaskId > playingTaskId) break;
                            _displayMessageQueue.RemoveFirst();
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() =>
                            {
                                var mainWindow = Application.Current.MainWindow as MainWindow;
                                if (mainWindow == null) return;
                                mainWindow.PutMessage(message);
                            }));
                        } while (_displayMessageQueue.Count > 0);
                    }
                    if (waiting && (!skipPlayingCheck || HasElapsed(FSSettings.SharedInstance().SilenceTimeout, timeoutCountdownStartedTicks)))
                    {
                        bool retry = false;
                        if (playingTaskId < 0) playingTaskId = GetCurrentTaskId();
                        if (playingTaskId > _currentTaskLastId)
                        {
                            waiting = false;
                        }
                        else if (!GetNowPlaying())
                        {
                            if (playingTaskId >= _currentTaskLastId) waiting = false;
                            else if (pausingTaskId != playingTaskId) pausingTaskId = playingTaskId;
                            else
                            {
                                /* Probably the last message has nothing to speak (e.g. symbol only) */
                                waiting = false;
                                playingTaskId = _currentTaskLastId;
                                retry = true;
                            }
                        }
                        if (!waiting)
                        {
                            _lastSpeechTicks = DateTime.Now.Ticks;
                        }
                        if (retry) goto retry;
                    }
                    Thread.Sleep(100);
                    //if(_messageQueue.Count > 0) Console.WriteLine("{0:D} messages pending", _messageQueue.Count);
                }
            }).ConfigureAwait(false);
        }
        public static int SpeechText(string text, int messageId, bool isQuotedMessage, out string errorMessage)
        {
            string query = FSSettings.SharedInstance().GetHttpQueryURL(text, isQuotedMessage);
            //Console.WriteLine(query);
            int ret = -1;
            errorMessage = null;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(query);
                req.Timeout = 1000;
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                using (resp)
                {
                    StreamReader sr = new StreamReader(resp.GetResponseStream());
                    try
                    {
                        JObject obj = JsonConvert.DeserializeObject<JObject>(sr.ReadToEnd());
                        ret = obj["taskId"].ToObject<int>();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Console.WriteLine(ex.Message);
            }
            return ret;
        }
        public static int SpeechText(string text, int messageId, bool isQuotedMessage)
        {
            return SpeechText(text, messageId, isQuotedMessage, out _);
        }
        public static void PushMessage(FSMessage message)
        {
            FSDownloadTaskManager.QueueTask(message.MediaDownloadTask);
            lock (_messageQueue)
            {
                _messageQueue.AddLast(message);
                if (message.MessageId > 0) _messageIds.Add(message.MessageId);
            }
        }
        public static void PushMessages(List<FSMessage> messages)
        {
            lock (_messageQueue)
            {
                foreach (var message in messages)
                {
                    FSDownloadTaskManager.QueueTask(message.MediaDownloadTask);
                    _messageQueue.AddLast(message);
                    if (message.MessageId > 0) _messageIds.Add(message.MessageId);
                }
            }
        }
        public static void Quit()
        {
            _quit = true;
        }
        public static void UpdateStatus()
        {
            int count = _messageIds.Count;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action)(() =>
            {
                var mainWindow = Application.Current.MainWindow as MainWindow;
                if (mainWindow == null) return;
                string msg = (count == 0) ? "処理待ちなし" : $"{count} レス 処理待ち";
                mainWindow.SetSpeechStatus(msg);
            }));
        }
        public static List<FSMessage> GetPendingMessages()
        {
            var list = new List<FSMessage>();
            lock (_messageQueue)
            {
                var entry = _messageQueue.First;
                while (entry != null)
                {
                    var message = entry.Value;
                    var next = entry.Next;
                    if (message.MessageId > 0 && message.MessageId < 100000)
                    {
                        _messageQueue.Remove(entry);
                        _messageIds.Remove(message.MessageId);
                        list.Add(message);
                    }
                    entry = next;
                }
                UpdateStatus();
            }
            FSDownloadTaskManager.DequeAllTasks();
            foreach (var message in list)
            {
                var task = message.MediaDownloadTask;
                if (task == null) continue;
                if (task.Status != TaskStatus.Created)
                {
                    FSDownloadTaskManager.ReleaseSemaphore();
                    message.MediaDownloadTask = FSDownloadTaskManager.CreateTaskForURL(message.MediaURL);
                }
            }
            return list;
        }
        public static void SkipToNextMessage()
        {
            _skip = true;
        }
        private static bool GetNowPlaying()
        {
            bool ret = false;
            string query = $"{FSSettings.SharedInstance().HttpAddress}getnowplaying";
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(query);
                req.Timeout = 1000;
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                using (resp)
                {
                    StreamReader sr = new StreamReader(resp.GetResponseStream());
                    try
                    {
                        JObject obj = JsonConvert.DeserializeObject<JObject>(sr.ReadToEnd());
                        ret = obj["nowPlaying"].ToObject<bool>();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return ret;
        }
        private static int GetCurrentTaskId()
        {
            int ret = -1;
            string query = $"{FSSettings.SharedInstance().HttpAddress}getnowtaskid";
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(query);
                req.Timeout = 1000;
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                using (resp)
                {
                    StreamReader sr = new StreamReader(resp.GetResponseStream());
                    try
                    {
                        JObject obj = JsonConvert.DeserializeObject<JObject>(sr.ReadToEnd());
                        ret = obj["nowTaskId"].ToObject<int>();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return ret;
        }
        private static void CancelPlaying()
        {
            string query = $"{FSSettings.SharedInstance().HttpAddress}clear";
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(query);
                req.Timeout = 1000;
                ((HttpWebResponse)req.GetResponse()).Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            query = $"{FSSettings.SharedInstance().HttpAddress}skip";
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(query);
                req.Timeout = 1000;
                ((HttpWebResponse)req.GetResponse()).Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Thread.Sleep(500);
        }
        private static bool HasElapsed(double timeInMs, Int64 baseInTicks, Int64 targetInTicks = 0)
        {
            if (targetInTicks == 0) targetInTicks = DateTime.Now.Ticks;
            return (targetInTicks - baseInTicks) / TimeSpan.TicksPerMillisecond >= timeInMs;
        }
    }
}