﻿using System;
using System.Collections.Generic;

namespace FutabaSpeech
{
	public class FSFutabaJson
	{
		public string die { get; set; }
		public Dictionary<string, FSFutabaJson.Res> res { get; set; }
		public string dielong { get; set; }
		public int old { get; set; }
		public string maxres { get; set; }

		public class Res
		{
			public string com { get; set; }
			public string src { get; set; }
			public string thumb { get; set; }
			public int rsc { get; set; }
			public int fsize { get; set; }
			public int w { get; set; }
			public int h { get; set; }
			public string del { get; set; }
			public string name { get; set; }
			public string email { get; set; }
		}
	}
}
