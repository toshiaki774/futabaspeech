﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.ComponentModel;
using Newtonsoft.Json;

namespace FutabaSpeech
{
    public class FSMessageFilter
    {
        public enum OperationType
        {
            Block = 0,
            Allow = 1,
        }
        public enum FilterType
        {
            All = 0,
            Futaba = 1,
            Twitch = 2,
            YouTube = 3,
        }
        public enum FilterTarget
        {
            Name = 0,
            Mail = 1,
            Message = 2,
        }
        public enum FilterCondition
        {
            Equals = 0,
            NotEquals = 1,
            Starts = 2,
            Ends = 3,
            Contains = 4,
            NotContains = 5,
            Regex = 6,
        }
        static Dictionary<OperationType, string> _operationSelection = new Dictionary<OperationType, string>
        {
            { OperationType.Block, "拒否" },
            { OperationType.Allow, "許可" },
        };
        static Dictionary<FilterType, string> _typeSelection = new Dictionary<FilterType, string>
        {
            { FilterType.All, "全て" },
            { FilterType.Futaba, "ふたば" },
            { FilterType.Twitch, "Twitch" },
            { FilterType.YouTube, "YouTube" },
        };
        static Dictionary<FilterTarget, string> _targetSelection = new Dictionary<FilterTarget, string>
        {
            { FilterTarget.Mail, "メール" },
            { FilterTarget.Name, "名前" },
            { FilterTarget.Message, "本文" },
        };
        static Dictionary<FilterCondition, string> _conditionSelection = new Dictionary<FilterCondition, string>
        {
            { FilterCondition.Equals, "と一致" },
            { FilterCondition.NotEquals, "と一致しない" },
            { FilterCondition.Starts, "で始まる" },
            { FilterCondition.Ends, "で終わる" },
            { FilterCondition.Contains, "を含む" },
            { FilterCondition.NotContains, "を含まない" },
            { FilterCondition.Regex, "正規表現にマッチ" },
        };
        public ObservableCollection<FSMessageFilterRule> Rules { get; set; } = new ObservableCollection<FSMessageFilterRule>();
        public OperationType DefaultOperation { get; set; } = OperationType.Allow;
        [JsonIgnore]
        public Dictionary<OperationType, string> OperationSelection { get; set; } = _operationSelection;
        public bool TestFutabaPost(FSFutabaJson.Res post, string messageText)
        {
            foreach (var rule in Rules)
            {
                if (rule.Match(post.name, post.email, messageText, 1)) return (rule.Operation != OperationType.Block);
            }
            return (DefaultOperation != OperationType.Block);
        }
        public bool TestTwitchChat(string name, string messageText)
        {
            foreach (var rule in Rules)
            {
                if (rule.Match(name, null, messageText, 2)) return (rule.Operation != OperationType.Block);
            }
            return (DefaultOperation != OperationType.Block);
        }
        public bool TestYTChat(string name, string messageText)
        {
            foreach (var rule in Rules)
            {
                if (rule.Match(name, null, messageText, 3)) return (rule.Operation != OperationType.Block);
            }
            return (DefaultOperation != OperationType.Block);
        }
        public int AddNewRule()
        {
            Rules.Add(new FSMessageFilterRule());
            return Rules.Count - 1;
        }
        public void RemoveRuleAtIndex(int index)
        {
            if (index < 0 || index > Rules.Count - 1) return;
            Rules.RemoveAt(index);
        }
        public void MoveUpRuleAtIndex(int index)
        {
            if (index <= 0) return;
            Rules.Move(index, index - 1);
        }
        public void MoveDownRuleAtIndex(int index)
        {
            if (index >= Rules.Count - 1) return;
            Rules.Move(index, index + 1);
        }

        public class FSMessageFilterRule : INotifyPropertyChanged
        {
            private FilterCondition _condition = FilterCondition.Equals;
            private string _text = "";
            private Regex _regex;
            public event PropertyChangedEventHandler PropertyChanged;
            public bool Enabled { get; set; } = true;
            public FilterType Type { get; set; } = FilterType.All;
            public FilterTarget Target { get; set; } = FilterTarget.Message;
            public FilterCondition Condition
            {
                get { return _condition; }
                set
                {
                    _condition = value;
                    if (value == FilterCondition.Regex)
                    {
                        try
                        {
                            _regex = new Regex(_text);
                        }
                        catch (Exception ex)
                        {
                            _regex = null;
                        }
                    }
                    NotifyPropertyChanged(EventArgsCache.IsInvalidRegexPropertyChanged);
                }
            }
            public string Text
            {
                get { return _text; }
                set
                {
                    _text = value;
                    if (Condition == FilterCondition.Regex)
                    {
                        try
                        {
                            _regex = new Regex(value);
                        }
                        catch (Exception ex)
                        {
                            _regex = null;
                        }
                        NotifyPropertyChanged(EventArgsCache.IsInvalidRegexPropertyChanged);
                    }
                }
            }
            public OperationType Operation { get; set; } = OperationType.Block;
            [JsonIgnore]
            public bool IsInvalidRegex
            {
                get { return (_condition == FilterCondition.Regex && _regex == null); }
            }
            public bool Match(string name, string mail, string messageText, int source)
            {
                string text = null;
                bool match = false;
                if (!Enabled) return false;
                if (Type != 0 && Type != (FilterType)source) return false;
                switch (Target)
                {
                    case FilterTarget.Name:
                        text = name;
                        break;
                    case FilterTarget.Mail:
                        text = mail;
                        break;
                    case FilterTarget.Message:
                        text = messageText;
                        break;
                }
                if (text == null) return false;
                switch (Condition)
                {
                    case FilterCondition.Equals:
                        if (text == Text) match = true;
                        break;
                    case FilterCondition.NotEquals:
                        if (text != Text) match = true;
                        break;
                    case FilterCondition.Starts:
                        if (text.StartsWith(Text)) match = true;
                        break;
                    case FilterCondition.Ends:
                        if (text.EndsWith(Text)) match = true;
                        break;
                    case FilterCondition.Contains:
                        if (text.Contains(Text)) match = true;
                        break;
                    case FilterCondition.NotContains:
                        if (!text.Contains(Text)) match = true;
                        break;
                    case FilterCondition.Regex:
                        if (_regex != null && _regex.IsMatch(text)) match = true;
                        break;
                }
                return match;
            }
            private void NotifyPropertyChanged(PropertyChangedEventArgs e)
            {
                PropertyChanged?.Invoke(this, e);
            }
            [JsonIgnore]
            public Dictionary<OperationType, string> OperationSelection { get; set; } = FSMessageFilter._operationSelection;
            [JsonIgnore]
            public Dictionary<FilterType, string> TypeSelection { get; set; } = FSMessageFilter._typeSelection;
            [JsonIgnore]
            public Dictionary<FilterTarget, string> TargetSelection { get; set; } = FSMessageFilter._targetSelection;
            [JsonIgnore]
            public Dictionary<FilterCondition, string> ConditionSelection { get; set; } = FSMessageFilter._conditionSelection;

            internal static class EventArgsCache
            {
                internal static readonly PropertyChangedEventArgs IsInvalidRegexPropertyChanged = new PropertyChangedEventArgs("IsInvalidRegex");
            }
        }
    }
}
