﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FutabaSpeech
{
    public class FSYTChatReceiver
    {
        public bool Running { get; set; } = false;
        public int ChatType
        {
            get
            {
                return _chatType;
            }
            set
            {
                if (_chatType != value)
                {
                    _chatType = value;
                    Interlocked.Exchange(ref _newChatType, value);
                }
            }
        }
        private string _channelId;
        private long _lastCheck;
        private int _chatType = 0;
        private int _newChatType = -1;
        private int _messageId = 10000000;
        private MainWindow _mainWindow;
        private static DateTimeOffset _baseDt = new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero);
        private readonly CountdownEvent _contdownEvent = new CountdownEvent(1);
        private static string _userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36 Edg/123.0.2420.97";
        public FSYTChatReceiver(string channelId, MainWindow window)
        {
            _channelId = channelId;
            _mainWindow = window;
        }
        private long GetCurrentUnixTime()
        {
            return (DateTimeOffset.Now - _baseDt).Ticks / (TimeSpan.TicksPerMillisecond / 1000);
        }
        private string GetLiveVideoIdFromChannelId(string channelId)
        {
            HttpWebResponse resp = null;
            string ytInitialPlayerResponse = null;
            string ytInitialData = null;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create($"https://www.youtube.com/{channelId}/live");
                req.Timeout = 5000;
                req.UserAgent = _userAgent;
                resp = (HttpWebResponse)req.GetResponse();
                string html = new StreamReader(resp.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                var match = Regex.Match(html, "var\\s+ytInitialPlayerResponse\\s*=\\s*(.+?})(?:\"\\))?;", RegexOptions.Singleline);
                if (match.Success)
                {
                    ytInitialPlayerResponse = match.Groups[1].Value;
                }
                match = Regex.Match(html, "var\\s+ytInitialData\\s*=\\s*(.+?})(?:\"\\))?;", RegexOptions.Singleline);
                if (match.Success)
                {
                    ytInitialData = match.Groups[1].Value;
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                if (resp != null) resp.Close();
            }
            if (ytInitialPlayerResponse != null && ytInitialData != null)
            {
                bool isOnline = false;
                try
                {
                    var json = JObject.Parse(ytInitialPlayerResponse);
                    var status = json.SelectToken("playabilityStatus.status");
                    if (status != null) isOnline = (status.Value<string>() == "OK");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                if (isOnline)
                {
                    try
                    {
                        var json = JObject.Parse(ytInitialData);
                        var videoId = json.SelectToken("currentVideoEndpoint.watchEndpoint.videoId");
                        if (videoId != null) return videoId.Value<string>();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
                else
                {
                }
            }
            return null;
        }
        private JObject GetChatRequestJsonForChannel(string channelId, int type)
        {
            HttpWebResponse resp = null;
            string ytInitialData = null;
            string ytcfg = null;
            string continuation = null;
            string apiKey = "AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8";
            string clientVersion = "2.20240419.01.00";
            var videoId = GetLiveVideoIdFromChannelId(channelId);
            if (videoId == null) return null;
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create($"https://www.youtube.com/live_chat?v={videoId}&is_popout=1");
                req.Timeout = 5000;
                req.UserAgent = _userAgent;
                resp = (HttpWebResponse)req.GetResponse();
                string html = new StreamReader(resp.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                var match = Regex.Match(html, "window\\[\"ytInitialData\"\\]\\s*=\\s*(.+?})(?:\"\\))?;", RegexOptions.Singleline);
                if (match.Success)
                {
                    ytInitialData = match.Groups[1].Value;
                }
                match = Regex.Match(html, "ytcfg\\.set\\(({.+?})(?:\\));", RegexOptions.Singleline);
                if (match.Success)
                {
                    ytcfg = match.Groups[1].Value;
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                if (resp != null) resp.Close();
            }
            if (ytInitialData == null || ytcfg == null) return null;
            try
            {
                var json = JObject.Parse(ytInitialData);
                var reloadContinuations = json.SelectToken("contents.liveChatRenderer.header.liveChatHeaderRenderer.viewSelector.sortFilterSubMenuRenderer.subMenuItems") as JArray;
                if (reloadContinuations != null)
                {
                    JToken token = null;
                    if (type == 0) token = reloadContinuations[0].SelectToken("continuation.reloadContinuationData.continuation");
                    else token = reloadContinuations[1].SelectToken("continuation.reloadContinuationData.continuation");
                    continuation = token.Value<string>();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            try
            {
                var json = JObject.Parse(ytcfg);
                var keyToken = json.SelectToken("INNERTUBE_API_KEY");
                var versionToken = json.SelectToken("INNERTUBE_CLIENT_VERSION");
                if (keyToken != null) apiKey = keyToken.Value<string>();
                if (versionToken != null) clientVersion = versionToken.Value<string>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            if (continuation == null) return null;
            JObject obj = new JObject
            {
                ["context"] = new JObject
                {
                    ["client"] = new JObject
                    {
                        ["clientName"] = "WEB",
                        ["clientVersion"] = clientVersion,
                        ["timeZone"] = "Asia/Tokyo",
                        ["utcOffsetMinutes"] = 540,
                    }
                },
                ["continuation"] = continuation,
                ["apiKey"] = apiKey
            };
            return obj;
        }
        public async Task Run()
        {
            Running = true;
            int status = await Task.Run(() =>
            {
                _lastCheck = GetCurrentUnixTime();
                var requestBodyJson = GetChatRequestJsonForChannel(_channelId, ChatType);
                _newChatType = -1;
                if (requestBodyJson == null) return -1;
                _contdownEvent.Reset();
                var chatURL = $"https://www.youtube.com/youtubei/v1/live_chat/get_live_chat?key={requestBodyJson["apiKey"].Value<string>()}";
                var messageFilter = FSSettings.SharedInstance().ContextProps.Filter;
                while (Running)
                {
                    HttpWebResponse resp = null;
                    string jsonStr = null;
                    try
                    {
                        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(chatURL);
                        req.Method = "POST";
                        req.Timeout = 5000;
                        req.UserAgent = _userAgent;
                        req.ContentType = "application/json";
                        using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                        {
                            streamWriter.Write(requestBodyJson.ToString());
                        }
                        resp = (HttpWebResponse)req.GetResponse();
                        jsonStr = new StreamReader(resp.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                    }
                    catch (WebException ex)
                    {
                        Console.WriteLine(ex.Message);
                        HttpStatusCode statusCode = 0;
                        if (ex.Status == WebExceptionStatus.ProtocolError)
                        {
                            statusCode = statusCode = ((HttpWebResponse)ex.Response).StatusCode;
                        }
                        if (ex.Status != WebExceptionStatus.Timeout && statusCode < HttpStatusCode.InternalServerError)
                        {
                            return -2;
                        }
                        else
                        {
                            _contdownEvent.Wait(5000);
                            _contdownEvent.Reset();
                            continue;
                        }
                    }
                    finally
                    {
                        if (resp != null) resp.Close();
                    }
                    if (jsonStr == null) return -3;
                    try
                    {
                        var now = DateTime.Now.Ticks;
                        var json = JObject.Parse(jsonStr);
                        var actions = json.SelectToken("continuationContents.liveChatContinuation.actions") as JArray;
                        if (actions != null)
                        {
                            long lastCheck = 0;
                            var newMessages = new List<FSMessage>();
                            foreach (JObject action in actions)
                            {
                                var items = action.SelectToken("addChatItemAction.item") as JObject;
                                if (items == null) continue;
                                JObject renderer = null;
                                foreach (var item in items)
                                {
                                    if (item.Key == "liveChatTextMessageRenderer" || item.Key == "liveChatPaidMessageRenderer" || item.Key == "liveChatPaidStickerRenderer")
                                    {
                                        renderer = item.Value as JObject;
                                        break;
                                    }
                                }
                                if (renderer == null) continue;
                                var timestamp = renderer["timestampUsec"];
                                if (timestamp != null)
                                {
                                    long value = timestamp.Value<long>();
                                    if (value < _lastCheck) continue;
                                    if (value > lastCheck) lastCheck = value;
                                }
                                var sb = new StringBuilder();
                                var messageRuns = renderer.SelectToken("message.runs") as JArray;
                                var messageSimpleText = renderer.SelectToken("message.simpleText");
                                var authorName = renderer.SelectToken("authorName.simpleText");
                                var amount = renderer.SelectToken("purchaseAmountText.simpleText");
                                if (messageRuns != null)
                                {
                                    string previousEmojiId = null;
                                    foreach (JObject run in messageRuns)
                                    {
                                        var str = run["text"];
                                        if (str != null)
                                        {
                                            sb.Append(str.Value<string>());
                                            previousEmojiId = null;
                                        }
                                        else
                                        {
                                            var emoji = run["emoji"];
                                            if (emoji != null)
                                            {
                                                var emojiId = emoji["emojiId"];
                                                if (previousEmojiId == null || previousEmojiId != emojiId.Value<string>())
                                                {
                                                    var altText = emoji.SelectToken("image.accessibility.accessibilityData.label");
                                                    if (altText != null)
                                                    {
                                                        if (sb.Length > 0) sb.Append(" ");
                                                        sb.Append(altText.Value<string>());
                                                    }
                                                    previousEmojiId = emojiId.Value<string>();
                                                }
                                            }
                                            else previousEmojiId = null;
                                            //Console.WriteLine(run["emoji"].ToString());
                                        }
                                    }
                                }
                                else if (messageSimpleText != null)
                                {
                                    sb.Append(messageSimpleText.Value<string>());
                                }
                                if (amount != null)
                                {
                                    if (sb.Length > 0) sb.Append(" ");
                                    sb.Append(amount.Value<string>());
                                }
                                var messageText = sb.ToString();
                                if (messageText.Length > 0)
                                {
                                    if (FSSettings.SharedInstance().YoutubeReadingMode == 0 || !_mainWindow.HasActiveThread())
                                    {
                                        string name = null;
                                        if (authorName != null) name = authorName.Value<String>();
                                        if (messageFilter.TestYTChat(name, messageText))
                                        {
                                            var msg = new FSMessage(_messageId++, null, null);
                                            msg.AddLine(messageText);
                                            newMessages.Add(msg);
                                        }
                                    }
                                }
                                //Console.WriteLine(text.ToString());
                            }
                            if (lastCheck != 0) _lastCheck = lastCheck + 1;
                            if (newMessages.Count > 0)
                            {
                                FSSpeechClient.PushMessages(newMessages);
                                FSSpeechClient.UpdateStatus();
                            }
                        }
                        int timeout = 3000;
                        var continuationData = json.SelectToken("continuationContents.liveChatContinuation.continuations[0].timedContinuationData") as JObject;
                        if (continuationData == null)
                        {
                            continuationData = json.SelectToken("continuationContents.liveChatContinuation.continuations[0].invalidationContinuationData") as JObject;
                        }
                        else timeout = continuationData["timeoutMs"].Value<int>();
                        if (continuationData == null) break;
                        if (Interlocked.Exchange(ref _newChatType, -1) >= 0)
                        {
                            Console.WriteLine($"Changing chat type to {_chatType}");
                            requestBodyJson = GetChatRequestJsonForChannel(_channelId, _chatType);
                            if (requestBodyJson == null) return -2;
                        }
                        else
                        {
                            requestBodyJson["continuation"] = continuationData["continuation"].Value<string>();
                        }
                        var elapsed = (DateTime.Now.Ticks - now) / TimeSpan.TicksPerMillisecond;
                        if (elapsed < timeout)
                        {
                            //Console.WriteLine($"sleep {timeout - elapsed} msec");
                            _contdownEvent.Wait((int)(timeout - elapsed));
                            _contdownEvent.Reset();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        Console.WriteLine(chatURL);
                        return -3;
                    }
                }
                return 0;
            });
            _mainWindow.youtubeClientExited(status);
        }
        public void Pause()
        {
            Running = false;
            _contdownEvent.Signal();
        }
    }
}
