﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace FutabaSpeech
{
    public static class FSDownloadTaskManager
    {
        private static LinkedList<Task<MemoryStream>> _taskQueue = new LinkedList<Task<MemoryStream>>();
        public static bool Running;
        private static bool _quit;
        private static SemaphoreSlim _semaphore = new SemaphoreSlim(4);
        public static async Task Run()
        {
            Running = true;
            _quit = false;
            await Task.Run(() =>
            {
                while (Running && !_quit)
                {
                    if (_taskQueue.Count <= 0)
                    {
                        Thread.Sleep(100);
                        continue;
                    }
                    if (_semaphore.Wait(100))
                    {
                        lock (_taskQueue)
                        {
                            if (_taskQueue.Count <= 0)
                            {
                                _semaphore.Release();
                            }
                            else
                            {
                                var task = _taskQueue.First.Value;
                                _taskQueue.RemoveFirst();
                                task.Start();
                            }
                        }
                    }
                }
            }).ConfigureAwait(false);
        }
        public static void QueueTask(Task<MemoryStream> task)
        {
            if (task == null) return;
            if (task.Status != TaskStatus.Created) return;
            lock (_taskQueue)
            {
                _taskQueue.AddLast(task);
            }
        }
        public static void DequeAllTasks()
        {
            lock (_taskQueue)
            {
                _taskQueue.Clear();
            }
        }
        public static void ReleaseSemaphore()
        {
            _semaphore.Release();
        }
        public static void Quit()
        {
            _quit = true;
        }
        public static Task<MemoryStream> CreateTaskForURL(string url)
        {
            return new Task<MemoryStream>(() =>
            {
                int retryCount = 0;
                var ms = new MemoryStream();
                do
                {
                    bool shouldRetry = false;
                    try
                    {
                        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                        req.Timeout = 5000;
                        req.ReadWriteTimeout = 10000;
                        if (ms.Length > 0) req.AddRange(ms.Position);
                        using (var resp = req.GetResponse())
                        {
                            var stream = resp.GetResponseStream();
                            int read;
                            byte[] buffer = new byte[4096];
                            while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                ms.Write(buffer, 0, read);
                            }
                            ms.Position = 0;
                            return ms;
                        }
                    }
                    catch (WebException ex)
                    {
                        //Console.WriteLine($"{url}: {ex.Message}, {ex.Status.ToString()}");
                        if (ex.Status == WebExceptionStatus.Timeout)
                        {
                            if (_taskQueue.Count == 0) shouldRetry = true;
                        }
                    }
                    catch (IOException ex)
                    {
                        //Console.WriteLine($"{url}: {ex.ToString()}");
                        if (ms.Length > 0)
                        {
                            if (_taskQueue.Count == 0) shouldRetry = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine($"{url}: {ex.ToString()}");
                    }
                    finally
                    {
                        //_semaphore.Release();
                    }
                    if (!shouldRetry) break;
                    //Console.WriteLine($"Retrying {url} ({retryCount}, {ms.Length})");
                } while (retryCount++ < 5);
                if (ms.Length > 0)
                {
                    ms.Position = 0;
                    return ms;
                }
                return null;
            });
        }
    }
}
