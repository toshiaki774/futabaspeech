﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Win32;

namespace FutabaSpeech
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("user32.dll")]
        static extern Int32 FlashWindowEx(ref FLASHWINFO pwfi);
        [StructLayout(LayoutKind.Sequential)]
        public struct FLASHWINFO
        {
            public UInt32 cbSize;    // FLASHWINFO構造体のサイズ
            public IntPtr hwnd;      // 点滅対象のウィンドウ・ハンドル
            public UInt32 dwFlags;   // 以下の「FLASHW_XXX」のいずれか
            public UInt32 uCount;    // 点滅する回数
            public UInt32 dwTimeout; // 点滅する間隔（ミリ秒単位）
        }
        private FSFutabaCrawler _currentCrawler;
        private Dictionary<string, FSFutabaCrawler> _activeCrawlers;
        private double _preservedHeight;
        private FSCommentWindow _commentWindow;
        private FSIrcClient _ircClient;
        private FSMediaWindow _mediaWindow;
        private FSYTChatReceiver _ytClient;
        private ClipboardWatcher _clipboardWatcher;
        public MainWindow()
        {
            this.Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);
            InitializeComponent();
            this.DataContext = FSSettings.SharedInstance().ContextProps;
            _activeCrawlers = new Dictionary<string, FSFutabaCrawler>();
            var ver = GetType().Assembly.GetName().Version;
            var buildDate = new DateTime(2000, 1, 1).AddDays(ver.Build).AddSeconds(ver.Revision * 2);
            this.Title += $" {ver.Major}.{ver.Minor} build {buildDate.Year}{buildDate.Month:D2}{buildDate.Day:D2}";

            _commentWindow = new FSCommentWindow();
            _commentWindow.DataContext = FSSettings.SharedInstance().ContextProps;
            Slider1.DataContext = _commentWindow;
            Slider2.DataContext = _commentWindow;
            Slider3.DataContext = _commentWindow;
            Slider4.DataContext = _commentWindow;
            FontSelector.DataContext = _commentWindow;
            ColorPicker1.DataContext = _commentWindow;
            ColorPicker2.DataContext = _commentWindow;
            ColorPicker3.DataContext = _commentWindow;
            ColorPicker4.DataContext = _commentWindow;
            Check2.DataContext = _commentWindow;
            _mediaWindow = new FSMediaWindow();
            _mediaWindow.DataContext = FSSettings.SharedInstance().ContextProps;
            _ = FSSpeechClient.Run();
            _ircClient = new FSIrcClient(this);
            if (FSSettings.SharedInstance().ContextProps.TwitchEnabled)
            {
                Check1.IsChecked = true;
                ToggleIrcClient(Check1, null);
            }
            _ = FSDownloadTaskManager.Run();
            if (FSSettings.SharedInstance().ContextProps.YoutubeEnabled)
            {
                Check4.IsChecked = true;
                ToggleYoutubeClient(Check4, null);
            }
            RunBouyomiChan();
            _ = FSUpdateChecker.Check(ver, buildDate);
            this.Loaded += Window_Loaded;
        }
        public void SetAppStatus(string text)
        {
            StringBuilder message = new StringBuilder();
            if (_currentCrawler != null && _currentCrawler.Running)
            {
                message.Append("スレ");
            }
            if (_ircClient != null && _ircClient.Running && (FSSettings.SharedInstance().TwitchReadingMode == 0 || !HasActiveThread()))
            {
                if (message.Length != 0) message.Append("+");
                message.Append("Twitch");
            }
            if (_ytClient != null && _ytClient.Running && (FSSettings.SharedInstance().YoutubeReadingMode == 0 || !HasActiveThread()))
            {
                if (message.Length != 0) message.Append("+");
                message.Append("YouTube");
            }
            if (message.Length == 0) this.AppStatus.Content = "待機中";
            else
            {
                message.Append("読み上げ中");
                this.AppStatus.Content = message.ToString();
            }
            if (text.Length > 0)
            {
                this.StatusMessage.Content = text;
                this.StatusMessage.Visibility = Visibility.Visible;
            }
            else this.StatusMessage.Visibility = Visibility.Hidden;
        }
        public void SetSpeechStatus(string text)
        {
            this.SpeechStatus.Content = text;
        }
        public void TaskFinished(int status, bool dispose, string errorMsg)
        {
            if (status == -2)
            {
                SetAppStatus("スレッドが見つからないのでスレの読み上げを停止しました。");
                _activeCrawlers.Remove(_currentCrawler.ThreadURL);
            }
            else if (status == 1)
            {
                SetAppStatus("レス数が上限に達したのでスレの読み上げを停止しました。");
                _activeCrawlers.Remove(_currentCrawler.ThreadURL);
            }
            else if (status < 0)
            {
                SetAppStatus("接続エラーが発生したためスレの読み上げを停止しました。");
                if (dispose) _activeCrawlers.Remove(_currentCrawler.ThreadURL);
                MessageBox.Show($"スレッドに接続できないため、読み上げを停止します。{errorMsg}", "ネットワーク エラー", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else SetAppStatus("");
            _currentCrawler = null;
            this.FireButton.Content = "読み上げ開始";
            this.FireButton.IsEnabled = true;
            this.URLBox.IsEnabled = true;
        }
        public void PutMessage(FSMessage message)
        {
            if (message.MediaDownloadTask != null)
            {
                int messageId = message.MessageId;
                string type = message.UnknownMediaType;
                string originalMediaURL = message.OriginalMediaURL;
                _mediaWindow.CurrentMessageId = messageId;
                if (message.MediaDownloadTask.IsCompleted)
                {
                    FSDownloadTaskManager.ReleaseSemaphore();
                    if (FSSettings.SharedInstance().ContextProps.OpenMediaWindowAutomatically)
                    {
                        _mediaWindow.Show();
                        //if (!FSSettings.SharedInstance().ContextProps.DisplayViewsOnTop) _mediaWindow.Activate();
                    }
                    _mediaWindow.ShowImageFromStream(message.MediaDownloadTask.Result, type);
                    _mediaWindow.OriginalMediaURL = originalMediaURL;
                }
                else
                {
                    _mediaWindow.ShowLoadingIcon();
                    message.MediaDownloadTask.ContinueWith(previousTask =>
                    {
                        FSDownloadTaskManager.ReleaseSemaphore();
                        if (messageId != _mediaWindow.CurrentMessageId) return;
                        var ms = previousTask.Result;
                        this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() =>
                        {
                            if (FSSettings.SharedInstance().ContextProps.OpenMediaWindowAutomatically)
                            {
                                _mediaWindow.Show();
                                //if (!FSSettings.SharedInstance().ContextProps.DisplayViewsOnTop) _mediaWindow.Activate();
                            }
                            _mediaWindow.ShowImageFromStream(ms, type);
                            _mediaWindow.OriginalMediaURL = originalMediaURL;
                        }));
                    });
                }
                message.MediaDownloadTask = null;
            }
            if (_commentWindow == null) return;
            int idx = 0;
            int bulletType = 1;
            if (message.MessageId >= 10000000) bulletType = 3;
            else if (message.MessageId >= 100000) bulletType = 2;
            foreach (var line in message.Lines)
            {
                _commentWindow.PutLine(line.Item1, idx++ == 0 ? bulletType : 0, line.Item2);
            }
        }
        public bool HasActiveThread()
        {
            return _currentCrawler != null && _currentCrawler.Running;
        }
        public void youtubeClientExited(int status)
        {
            Check4.IsChecked = false;
            var statusText = "";
            if (status == -1)
            {
                MessageBox.Show("チャンネルが見つからないかチャットが有効なライブ配信が行われていません。", "YouTube 読み上げ", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (status == 0 && Check4.IsEnabled)
            {
                statusText = "YouTubeのライブ放送が終了しました。";
            }
            else if (status < 0)
            {
                FlashTaskBar();
                System.Media.SystemSounds.Beep.Play();
                statusText = "YouTubeのコメントサーバとの接続でエラーが発生しました。";
            }
            _ytClient = null;
            Check4.IsEnabled = true;
            SetAppStatus(statusText);
        }
        private void Fire(object sender, RoutedEventArgs e)
        {
            if (_currentCrawler != null)
            {
                this.FireButton.IsEnabled = false;
                _currentCrawler.Pause();
            }
            else
            {
                string url = this.URLBox.Text;
                var m = Regex.Match(url, @"^(?<url>(?<board>https?://[0-9a-z]+\.2chan\.net/[0-9a-z]+/)(res/[0-9]+.htm|futaba.php\?res=(?<res>[0-9]+)))(?:#(?<last>l)?(?<start>[0-9]+))?$", RegexOptions.IgnoreCase);
                if (!m.Success)
                {
                    MessageBox.Show("URLが不正です。", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                url = m.Groups["url"].Value;
                FSFutabaCrawler crawler;
                if (_activeCrawlers.TryGetValue(url, out crawler))
                {
                    if (!this.IsActive) System.Media.SystemSounds.Beep.Play();
                    var result = MessageBox.Show("読み上げたことのあるスレッドです。続きから読み上げますか？", "読み上げ確認", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    this.FireButton.Content = "停止";
                    this.URLBox.IsEnabled = false;
                    _currentCrawler = crawler;
                    int start = m.Groups["start"].Success ? int.Parse(m.Groups["start"].Value) : 0;
                    if (m.Groups["last"].Success) start = -start;
                    var _ = crawler.Run(start, result == MessageBoxResult.Yes);
                    SetAppStatus("");
                }
                else
                {
                    this.FireButton.Content = "停止";
                    this.URLBox.IsEnabled = false;
                    if (m.Groups["res"].Success) _currentCrawler = new FSFutabaCrawler(m.Groups["board"].Value, m.Groups["res"].Value);
                    else _currentCrawler = new FSFutabaCrawler(url);
                    _activeCrawlers.Add(url, _currentCrawler);
                    int start = m.Groups["start"].Success ? int.Parse(m.Groups["start"].Value) : 0;
                    if (m.Groups["last"].Success) start = -start;
                    var _ = _currentCrawler.Run(start, false);
                    SetAppStatus("");
                }
            }
        }

        private void AppWillQuit(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string url = this.URLBox.Text;
            var m = Regex.Match(url, "^(?<url>https?://[0-9a-z]+\\.2chan\\.net/[0-9a-z]+/res/[0-9]+.htm)(?:#(?<last>l)?(?<start>[0-9]+))?$", RegexOptions.IgnoreCase);
            if (m.Success)
            {
                url = m.Groups["url"].Value;
                FSFutabaCrawler crawler;
                if (_activeCrawlers.TryGetValue(url, out crawler))
                {
                    int n = crawler.NextMessageId;
                    if (n > 0)
                    {
                        this.URLBox.Text = $"{url}#{n}";
                    }
                }
            }
            if (_commentWindow != null) _commentWindow.Close();
            if (_mediaWindow != null) _mediaWindow.Close();
            FSSettings.SharedInstance().SaveSettings();
            _clipboardWatcher.Unwatch();
        }

        private void URL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return) Fire(sender, e);
        }

        private void TestSpeech(object sender, RoutedEventArgs e)
        {
            string errorMsg;
            if (FSSpeechClient.SpeechText("接続テストです。正しく接続できています。", 0, false, out errorMsg) < 0)
            {
                MessageBox.Show($"棒読みちゃんへの接続に失敗しました: {errorMsg}", "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ToggleSetting(object sender, RoutedEventArgs e)
        {
            if (SettingTab.IsVisible)
            {
                _preservedHeight = SettingTab.ActualHeight;
                SettingTab.Visibility = Visibility.Hidden;
                this.Height -= _preservedHeight;
                this.MinHeight = this.Height;
                this.MaxHeight = this.Height;
            }
            else
            {
                this.MinHeight = 0;
                this.MaxHeight = double.PositiveInfinity;
                this.Height += _preservedHeight;
                SettingTab.Visibility = Visibility.Visible;
            }
        }
        private void ToggleCommentWindow(object sender, RoutedEventArgs e)
        {
            if (_commentWindow.IsClosed) _commentWindow.Show();
            else _commentWindow.Hide();
        }
        private void ToggleMediaWindow(object sender, RoutedEventArgs e)
        {
            if (_mediaWindow.IsClosed)
            {
                _mediaWindow.Show();
                _mediaWindow.ShowPendingImage();
            }
            else _mediaWindow.Hide();
        }
        private void ColorPicker_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ComboBoxItem item = sender as ComboBoxItem;
            ComboBox picker = null;
            FSCommentColor color = null;
            if (item.Content == _commentWindow.TextColors.Last())
            {
                picker = ColorPicker1;
                color = _commentWindow.TextColors.Last();
            }
            else if (item.Content == _commentWindow.OutlineColors.Last())
            {
                picker = ColorPicker2;
                color = _commentWindow.OutlineColors.Last();
            }
            else if (item.Content == _commentWindow.BackgroundColors.Last())
            {
                picker = ColorPicker3;
                color = _commentWindow.BackgroundColors.Last();
            }
            else if (item.Content == _commentWindow.QuoteTextColors.Last())
            {
                picker = ColorPicker4;
                color = _commentWindow.QuoteTextColors.Last();
            }
            if (picker == null) return;
            var availableColors = picker.ItemsSource as ObservableCollection<FSCommentColor>;
            var cd = new System.Windows.Forms.ColorDialog();
            Color oldColor = (color.Color as SolidColorBrush).Color;
            cd.Color = System.Drawing.Color.FromArgb(oldColor.A, oldColor.R, oldColor.G, oldColor.B);
            cd.CustomColors = new int[] { (oldColor.B << 16) | (oldColor.G << 8) | oldColor.R };
            if (cd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (picker.SelectedIndex == availableColors.Count - 1) picker.SelectedIndex = 1;
                var brush = new SolidColorBrush(Color.FromArgb(cd.Color.A, cd.Color.R, cd.Color.G, cd.Color.B));
                color.Color = brush;
                picker.Items.Refresh();
            }
            picker.SelectedIndex = availableColors.Count - 1;
        }
        private void ToggleIrcClient(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            if (cb.IsChecked == true)
            {
                var channel = FSSettings.SharedInstance().ContextProps.TwitchChannel.Trim().TrimEnd('/').ToLower();
                if (channel.Length == 0)
                {
                    cb.IsChecked = false;
                    MessageBox.Show("チャンネル名が不正です。", "Twitch 読み上げ", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (_ircClient.Connect())
                {
                    var __ = _ircClient.Run();
                    _ircClient.Join($"#{channel}");
                    SetAppStatus("");
                }
                else
                {
                    cb.IsChecked = false;
                    MessageBox.Show("サーバに接続できません。", "Twitch 読み上げ", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                cb.IsEnabled = false;
                _ircClient.Running = false;
                SetAppStatus("");
            }
        }
        private void ToggleBouyomiLaunch(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            if (cb.IsChecked == false) return;
            if (string.IsNullOrEmpty(this.BouyomiPath.Text))
            {
                var list = System.Diagnostics.Process.GetProcessesByName("BouyomiChan");
                if (list.Length > 0)
                {
                    this.BouyomiPath.Text = list[0].MainModule.FileName;
                }
            }
        }
        private void ToggleYoutubeClient(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            if (cb.IsChecked == true)
            {
                var channel = FSSettings.SharedInstance().ContextProps.YoutubeChannel.Trim();
                if (channel.Length == 0 || channel[0] != '@')
                {
                    cb.IsChecked = false;
                    MessageBox.Show("チャンネル名が不正です。チャンネル名には半角@で始まるハンドルを入力してください。", "YouTube 読み上げ", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                _ytClient = new FSYTChatReceiver(channel, this);
                var __ = _ytClient.Run();
                SetAppStatus("");
            }
            else
            {
                if (_ytClient != null)
                {
                    cb.IsEnabled = false;
                    _ytClient.Pause();
                }
            }
        }
        private void SelectBouyomiPath(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "棒読みちゃん|BouyomiChan.exe|実行ファイル (*.exe)|*.exe";
            if (dialog.ShowDialog() == true)
            {
                this.BouyomiPath.Text = dialog.FileName;
                var list = System.Diagnostics.Process.GetProcessesByName("BouyomiChan");
                if (list.Length == 0)
                {
                    try
                    {
                        System.Diagnostics.Process.Start(dialog.FileName);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"棒読みちゃんの起動に失敗しました。{ex.Message}", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }
        private void RunBouyomiChan()
        {
            if (!FSSettings.SharedInstance().ContextProps.LaunchBouyomi) return;
            string path = FSSettings.SharedInstance().ContextProps.BouyomiPath;
            if (string.IsNullOrEmpty(path)) return;
            foreach (var process in System.Diagnostics.Process.GetProcessesByName("BouyomiChan"))
            {
                if (process.MainModule.FileName == path) return;
            }
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"棒読みちゃんの起動に失敗しました。{ex.Message}", "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void FlashTaskBar()
        {
            var info = new FLASHWINFO();
            info.cbSize = Convert.ToUInt32(Marshal.SizeOf(info));
            info.hwnd = new WindowInteropHelper(this).Handle;
            info.dwFlags = 15;
            info.uCount = 1;
            info.dwTimeout = 0;
            FlashWindowEx(ref info);
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SetAppStatus("");
        }
        private void SpeechStatus_MouseUp(object sender, MouseButtonEventArgs e)
        {
            FSSpeechClient.SkipToNextMessage();
        }
        private void SpeechStatus_QueryCursor(object sender, QueryCursorEventArgs e)
        {
            //if (!sender.ToString().Contains("レス 処理待ち")) return;
            e.Cursor = Cursors.Hand;
        }
        private void AddFilterRule(object sender, RoutedEventArgs e)
        {
            int idx = FSSettings.SharedInstance().ContextProps.Filter.AddNewRule();
            FilterList.SelectedIndex = idx;
            FilterList.ScrollIntoView(FilterList.SelectedItem);
        }
        private void RemoveFilterRule(object sender, RoutedEventArgs e)
        {
            int idx = FilterList.SelectedIndex;
            FSSettings.SharedInstance().ContextProps.Filter.RemoveRuleAtIndex(idx);
            if (!FilterList.Items.IsEmpty)
            {
                FilterList.SelectedIndex = Math.Min(idx, FilterList.Items.Count-1);
            }
        }
        private void MoveUpFilterRule(object sender, RoutedEventArgs e)
        {
            FSSettings.SharedInstance().ContextProps.Filter.MoveUpRuleAtIndex(this.FilterList.SelectedIndex);
        }
        private void MoveDownFilterRule(object sender, RoutedEventArgs e)
        {
            FSSettings.SharedInstance().ContextProps.Filter.MoveDownRuleAtIndex(this.FilterList.SelectedIndex);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _clipboardWatcher = new ClipboardWatcher(new WindowInteropHelper(this).Handle);
            _clipboardWatcher.ClipboardTextHandler += Clipboard_HasText;
            _clipboardWatcher.Watch();
        }
        private void Clipboard_HasText(object sender, EventArgs e)
        {
            if (this.IsActive || _currentCrawler != null || !FSSettings.SharedInstance().ContextProps.WatchClipboard) return;
            int trial = 0;
            string text = "";
            do
            {
                try
                {
                    text = Clipboard.GetText();
                    break;
                }
                catch (Exception ex)
                {
                    trial++;
                }
            } while (trial < 5);
            if (text.Length == 0) return;
            var m = Regex.Match(text, "^https?://[0-9a-z]+\\.2chan\\.net/[0-9a-z]+/res/[0-9]+.htm$", RegexOptions.IgnoreCase);
            if (m.Success)
            {
                this.URLBox.Text = text;
                Fire(sender, null);
            }
        }
    }

    public class ClipboardWatcher
    {
        [DllImport("user32.dll")]
        private static extern bool AddClipboardFormatListener(IntPtr hwnd);
        [DllImport("user32.dll")]
        private static extern bool RemoveClipboardFormatListener(IntPtr hwnd);
        private const int WM_DRAWCLIPBOARD = 0x031D;
        private IntPtr _hwnd;
        private HwndSource _hwndSource;
        private bool _watching;
        public event EventHandler ClipboardTextHandler;
        public ClipboardWatcher(IntPtr handle)
        {
            _watching = false;
            _hwnd = handle;
            _hwndSource = HwndSource.FromHwnd(_hwnd);
            _hwndSource.AddHook(WndProc);
        }
        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WM_DRAWCLIPBOARD)
            {
                if (Clipboard.ContainsText()) ClipboardTextHandler?.Invoke(this, EventArgs.Empty);
                handled = true;
            }
            return IntPtr.Zero;
        }
        public void Watch()
        {
            if (_watching) return;
            AddClipboardFormatListener(_hwnd);
            _watching = true;
        }
        public void Unwatch()
        {
            if (!_watching) return;
            RemoveClipboardFormatListener(_hwnd);
            _watching = false;
        }
    }
}
