﻿using System;
using System.Text;
using System.Web;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;

namespace FutabaSpeech
{
    public class FSMessage
    {
        private StringBuilder _lineBuffer;
        private string _quotePretext;
        public List<Tuple<string, bool>> Lines { get; private set; } = new List<Tuple<string, bool>>();
        public List<Tuple<StringBuilder, bool>> LineGroups { get; private set; } = new List<Tuple<StringBuilder, bool>>();
        public int MessageId { get; private set; }
        public int TaskId { get; set; }
        public Task<MemoryStream> MediaDownloadTask { get; set; }
        public string MediaURL { get; set; }
        public string OriginalMediaURL { get; set; }
        public string UnknownMediaType { get; set; }
        public FSMessage(int messageId, string pretext, string quotePretext)
        {
            MessageId = messageId;
            if (pretext != null && pretext != "")
            {
                _lineBuffer = new StringBuilder();
                _lineBuffer.AppendFormat("{0} ", pretext);
                AddNewGroup(_lineBuffer, false);
            }
            _quotePretext = quotePretext;
        }
        private void AddNewGroup(StringBuilder buffer, bool isQuotedLine)
        {
            LineGroups.Add(new Tuple<StringBuilder, bool>(buffer, isQuotedLine));
        }
        public void AddLine(string line)
        {
            bool isQuotedLine = false;
            if (line.StartsWith("&gt;"))
            {
                isQuotedLine = true;
                if (LineGroups.Count == 0 || !LineGroups[LineGroups.Count - 1].Item2)
                {
                    _lineBuffer = new StringBuilder();
                    AddNewGroup(_lineBuffer, true);
                }
            }
            else
            {
                if (LineGroups.Count == 0 || LineGroups[LineGroups.Count - 1].Item2)
                {
                    _lineBuffer = new StringBuilder();
                    AddNewGroup(_lineBuffer, false);
                }
            }
            string decodedLine = HttpUtility.HtmlDecode(line);
            Lines.Add(new Tuple<string, bool>(decodedLine, isQuotedLine));
            if (isQuotedLine && _quotePretext != null)
            {
                decodedLine = decodedLine.TrimStart('>');
                if (_quotePretext.Length != 0) _lineBuffer.AppendFormat("{0} ", _quotePretext);
            }
            _lineBuffer.AppendLine(decodedLine);


        }
    }
}