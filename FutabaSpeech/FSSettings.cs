﻿using System;
using System.IO;
using System.Text;
using System.Globalization;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using Newtonsoft.Json;

namespace FutabaSpeech
{
    public sealed class FSSettings
    {
        private static FSSettings _sharedInstance = new FSSettings();

        public static FSSettings SharedInstance()
        {
            return _sharedInstance;
        }
        public FSSettingsProps ContextProps { get; }
        public string HttpAddress
        {
            get
            {
                return string.Format("http://{0}:{1}/", ContextProps.HttpServer, ContextProps.HttpPort);
            }
        }
        public bool ShouldAlertNotFound
        {
            get { return ContextProps.ShouldAlertNotFound; }
        }
        public int SpeechInterval
        {
            get
            {
                return (int)(ContextProps.SpeechInterval*1000);
            }
        }
        public string NotFoundMessage
        {
            get
            {
                return ContextProps.NotFoundMessage;
            }
        }
        public bool SkipDeletedMessages
        {
            get { return ContextProps.SkipDeletedMessages; }
        }
        public bool TrimQuoteMark
        {
            get { return ContextProps.TrimQuoteMark; }
        }
        public string QuoteLinePretext
        {
            get { return ContextProps.QuoteLinePretext.Trim(); }
        }
        public int TwitchReadingMode
        {
            get { return ContextProps.TwitchReadingMode; }
        }
        public int YoutubeReadingMode
        {
            get { return ContextProps.YoutubeReadingMode; }
        }
        public bool ShouldAlertThreadIsFull
        {
            get { return ContextProps.ShouldAlertThreadIsFull; }
        }
        public string ThreadIsFullMessage
        {
            get
            {
                return ContextProps.ThreadIsFullMessage;
            }
        }
        public string GetDyingAlertMessage(int minutes)
        {
            return ContextProps.DyingAlertMessage.Replace("{Min}", minutes.ToString());
        }
        public string GetHttpQueryURL(string text, bool isQuotedMessage)
        {
            string encoded = Uri.EscapeDataString(text.TrimEnd());
            StringBuilder query = new StringBuilder();
            query.AppendFormat("{0}talk?text={1}", this.HttpAddress, encoded);
            if (isQuotedMessage)
            {
                if (ContextProps.QuoteVoiceType != 0)
                {
                    if (ContextProps.QuoteVoiceType != -1) query.AppendFormat("&voice={0}", ContextProps.QuoteVoiceType);
                    else query.AppendFormat("&voice={0}", ContextProps.CustomQuoteVoiceType);
                }
                if (ContextProps.QuoteVoiceVolume != 0)
                {
                    query.AppendFormat("&volume={0}", ContextProps.QuoteVoiceVolumeValue);
                }
                if (ContextProps.QuoteVoiceSpeed != 0)
                {
                    query.AppendFormat("&speed={0}", ContextProps.QuoteVoiceSpeedValue);
                }
                if (ContextProps.QuoteVoiceTone != 0)
                {
                    query.AppendFormat("&tone={0}", ContextProps.QuoteVoiceToneValue);
                }
            }
            else
            {
                if (ContextProps.VoiceType != 0)
                {
                    if (ContextProps.VoiceType != -1) query.AppendFormat("&voice={0}", ContextProps.VoiceType);
                    else query.AppendFormat("&voice={0}", ContextProps.CustomVoiceType);
                }
                if (ContextProps.VoiceVolume != 0)
                {
                    query.AppendFormat("&volume={0}", ContextProps.VoiceVolumeValue);
                }
                if (ContextProps.VoiceSpeed != 0)
                {
                    query.AppendFormat("&speed={0}", ContextProps.VoiceSpeedValue);
                }
                if (ContextProps.VoiceTone != 0)
                {
                    query.AppendFormat("&tone={0}", ContextProps.VoiceToneValue);
                }
            }
            return query.ToString();
        }
        public string GetPretext(int resNum, bool isImageRes)
        {
            if (!ContextProps.InsertPretext) return "";
            string text = (isImageRes ? ContextProps.ImagePretext : ContextProps.Pretext).Trim();
            if (text.Length == 0) return "";
            return $"{text.Replace("{Num}", resNum.ToString())} ";
        }
        public bool isDyingThread(int old, string dielong, out DateTime die)
        {
            bool dying = false;
            die = DateTime.MinValue;
            if (!ContextProps.ShouldAlertDying) return false;
            try
            {
                die = DateTime.ParseExact(dielong, "ddd, dd MMM yyyy HH:mm:ss GMT", CultureInfo.CreateSpecificCulture("en-US"));
                if (ContextProps.DyingAlertType == 0)
                {
                    int min;
                    if (!int.TryParse(ContextProps.DyingAlertMin, out min)) min = 5;
                    if (die.Ticks - DateTime.Now.Ticks < TimeSpan.TicksPerMinute * min) dying = true;
                }
                else
                {
                    if (old != 0) dying = true;
                }
            }
            catch (FormatException)
            {
            }
            return dying;
        }
        public void SaveSettings()
        {
            string jsonData = JsonConvert.SerializeObject(ContextProps, Formatting.Indented);
            using (var sw = new StreamWriter(@"settings.json", false, System.Text.Encoding.UTF8))
            {
                sw.Write(jsonData);
            }
        }
        public string GetUnreadAlertMessage(int count)
        {
            return ContextProps.UnreadAlertMessage.Replace("{Num}", count.ToString());
        }
        public int SilenceTimeout
        {
            get
            {
                return (int)(ContextProps.SilenceTimeout * 1000);
            }
        }
        private FSSettings()
        {
            try
            {
                using (var sr = new StreamReader("settings.json", System.Text.Encoding.UTF8))
                {
                    string jsonData = sr.ReadToEnd();
                    ContextProps = JsonConvert.DeserializeObject<FSSettingsProps>(jsonData);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            if (ContextProps == null) ContextProps = new FSSettingsProps();
        }

        public enum FSControlSelection {
            Value0,
            Value1,
            Value2,
            ValueEx = -1
        }
        public class FSSettingsProps
        {
            public string HttpServer { get; set; } = "localhost";
            public int HttpPort { get; set; } = 50080;
            public double SpeechInterval { get; set; } = 3.0;
            public bool ShouldAlertDying { get; set; } = false;
            public int DyingAlertType { get; set; } = 0;
            public string DyingAlertMessage { get; set; } = "あと{Min}分ほどでスレが落ちます。";
            public string DyingAlertMin { get; set; } = "5";
            public bool ShouldAlertNotFound { get; set; } = true;
            public string NotFoundMessage { get; set; } = "スレッドが見つかりません。読み上げを停止します。";
            public bool ShouldAlertThreadIsFull { get; set; } = true;
            public string ThreadIsFullMessage { get; set; } = "レス数が上限に達しました。読み上げを停止します。";
            public int VoiceVolume { get; set; } = 0;
            public int VoiceSpeed { get; set; } = 0;
            public int VoiceTone { get; set; } = 0;
            public int QuoteVoiceVolume { get; set; } = 0;
            public int QuoteVoiceSpeed { get; set; } = 0;
            public int QuoteVoiceTone { get; set; } = 0;
            public int VoiceType { get; set; } = 1;
            public string CustomVoiceType { get; set; } = "10001";
            public int VoiceVolumeValue { get; set; } = 100;
            public int VoiceSpeedValue { get; set; } = 100;
            public int VoiceToneValue { get; set; } = 100;
            public int QuoteVoiceType { get; set; } = 3;
            public string CustomQuoteVoiceType { get; set; } = "10001";
            public int QuoteVoiceVolumeValue { get; set; } = 100;
            public int QuoteVoiceSpeedValue { get; set; } = 100;
            public int QuoteVoiceToneValue { get; set; } = 100;
            public bool InsertPretext { get; set; } = false;
            public string Pretext { get; set; } = "{Num}";
            public string ImagePretext { get; set; } = "{Num} 画像レス";
            public bool SkipDeletedMessages { get; set; } = true;
            public bool TrimQuoteMark { get; set; } = false;
            public string QuoteLinePretext { get; set; } = "";
            public string CommentTextFont { get; set; } = "MS PGothic";
            public double CommentTextSize { get; set; } = 24;
            public double CommentLineSpacing { get; set; } = 5;
            public int CommentLines { get; set; } = 5;
            public string CommentTextColor { get; set; } = "#ffffff";
            public string CommentQuoteTextColor { get; set; } = "#ffffff";
            public string CommentOutlineColor { get; set; } = "#000000";
            public string CommentBackgroundColor { get; set; } = "#ff00ff";
            public bool CommentUseBoldFont { get; set; } = false;
            public double MainWindowLeft { get; set; } = Double.NaN;
            public double MainWindowTop { get; set; } = Double.NaN;
            public double CommentWindowLeft { get; set; } = Double.NaN;
            public double CommentWindowTop { get; set; } = Double.NaN;
            public double CommentWindowWidth { get; set; } = 800;
            public string TwitchChannel { get; set; } = "";
            public int TwitchReadingMode { get; set; } = 0;
            public bool TwitchEnabled { get; set; } = false;
            public string YoutubeChannel { get; set; } = "";
            public int YoutubeReadingMode { get; set; } = 0;
            public bool YoutubeEnabled { get; set; } = false;
            public double MediaWindowLeft { get; set; } = Double.NaN;
            public double MediaWindowTop { get; set; } = Double.NaN;
            public double MediaWindowWidth { get; set; } = 512;
            public double MediaWindowHeight { get; set; } = 512;
            public bool OpenMediaWindowAutomatically { get; set; } = false;
            public bool DisplayViewsOnTop { get; set; } = false;
            public bool ApplyMosaicAutomatically { get; set; } = false;
            public string LastURL { get; set; } = "";
            public int CommentViewTimeout { get; set; } = 60;
            public bool CommentWindowVisibility { get; set; } = false;
            public bool LaunchBouyomi { get; set; } = false;
            public string BouyomiPath { get; set; } = "";
            public bool ShouldAlertUnreadMessageCount { get; set; } = false;
            public int UnreaAlertThreshold { get; set; } = 20;
            public string UnreadAlertMessage { get; set; } = "未読のレスが{Num}個あります。";
            public double OutlineThickness { get; set; } = 0.09;
            public double SilenceTimeout { get; set; } = 1.0;
            public FSMessageFilter Filter { get; set; } = new FSMessageFilter();
            public bool CheckForUpdate { get; set; } = true;
            public bool WatchClipboard { get; set; } = false;
            [JsonIgnore]
            public Dictionary<int, string> VoiceSelection { get; set; } = new Dictionary<int, string>
            {
                { 0, "デフォルト" },
                { 1, "女性1" },
                { 2, "女性2" },
                { 3, "男性1" },
                { 4, "男性2" },
                { 5, "中性" },
                { 6, "ロボット" },
                { 7, "機械1" },
                { 8, "機械2" },
                { -1, "その他" },
            };
        }
    }
    public class IntToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var parameterString = parameter as string;
            if (null == parameterString)
                return DependencyProperty.UnsetValue;

            bool negate = parameterString.Length > 0 && parameterString[0] == '!';
            if (negate) parameterString = parameterString.Substring(1);
            var parameterValue = int.Parse(parameterString);
            return parameterValue.Equals(value) ^ negate;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var parameterString = parameter as string;
            bool negate = parameterString.Length > 0 && parameterString[0] == '!';
            if (negate) parameterString = parameterString.Substring(1);
            if (null == parameterString)
                return DependencyProperty.UnsetValue;

            if (true.Equals(value) ^ negate)
                return int.Parse(parameterString);
            else
                return DependencyProperty.UnsetValue;
        }
    }
    public class BoolNegativeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(value is bool && (bool)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(value is bool && (bool)value);
        }

    }
}