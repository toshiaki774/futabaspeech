﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Linq;
using System.Xml.Linq;

namespace FutabaSpeech
{
    public static class FSUpdateChecker
    {
        private static readonly string feedURL = @"https://gitlab.com/toshiaki774/futabaspeech/-/releases.atom";
        private static readonly string releaseURL = @"https://gitlab.com/toshiaki774/futabaspeech/-/releases";
        public static async Task Check(Version ver, DateTime buildDate)
        {
            string newVersion = await Task.Run(() =>
            {
                if (!FSSettings.SharedInstance().ContextProps.CheckForUpdate) return null;
                try
                {
                    XDocument xml = XDocument.Load(feedURL);
                    var ns = xml.Root.Name.Namespace;
                    var latestRelease = xml.Root.Descendants(XName.Get("entry", ns.ToString())).FirstOrDefault();
                    if (latestRelease != null)
                    {
                        string version = latestRelease.Element(XName.Get("title", ns.ToString()))?.Value;
                        if (String.IsNullOrEmpty(version) || version[0] != 'v') return null;
                        var components = version.Substring(1).Split('-');
                        if (components.Length != 2) return null;
                        var majorAndMinor = components[0].Split('.');
                        if (majorAndMinor.Length != 2) return null;
                        int currentNumber = ver.Major*1000 + ver.Minor;
                        int newNumber = int.Parse(majorAndMinor[0])*1000 + int.Parse(majorAndMinor[1]);
                        if (newNumber > currentNumber) return version;
                        if (newNumber == currentNumber && String.Compare(components[1], $"{buildDate.Year}{buildDate.Month:D2}{buildDate.Day:D2}") > 0) return version;
                    }
                }
                catch (Exception) { }
                return null;
            });
            if (newVersion != null)
            {
                var ret = MessageBox.Show($"新しいバージョン {newVersion} があります。ダウンロードページを開きますか?", "アップデートの確認", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (ret == MessageBoxResult.Yes)
                {
                    try
                    {
                        System.Diagnostics.Process.Start(releaseURL);
                    }
                    catch (Exception) { }
                }
            }
        }
    }
}