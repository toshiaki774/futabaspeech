﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Windows;
using Newtonsoft.Json;

namespace FutabaSpeech
{
    public class FSFutabaCrawler
    {
        private string _baseQueryURL;
        private Random _rng;
        private string _lastResNum;
        private bool _announcedThreadIsDying;
        private readonly CountdownEvent _contdownEvent = new CountdownEvent(1);
        private List<FSMessage> _pendingMessages;
        private string _baseURL;
        private int _lastCommittedMessageId;
        private bool _checkThreadAvailabilityByRequest;
        public bool Running { get; private set; }
        public string ThreadURL { get; private set; }
        public int NextMessageId
        {
            get {
                if (_pendingMessages == null) return _lastCommittedMessageId + 1;
                if (_pendingMessages.Count == 0) return _lastCommittedMessageId + 1;
                return _pendingMessages[0].MessageId;
            }
        }
        enum FSFutabaConnectionError
        {
            Timeout = -1,
            NotFound = -2,
            Unknown = -3,
        }
        public FSFutabaCrawler(string threadURL)
        {
            string[] array = threadURL.Split('/');
            _baseQueryURL = string.Concat(new string[]{
                array[0],
                "//",
                array[2],
                "/",
                array[3],
                "/futaba.php?mode=json&res=",
                Path.GetFileNameWithoutExtension(array[5])
            });
            _baseURL = string.Concat(new string[]{
                array[0],
                "//",
                array[2],
            });
            ThreadURL = threadURL;
            _rng = new Random();
            _lastResNum = "";
            _checkThreadAvailabilityByRequest = true;
        }
        public FSFutabaCrawler(string boardURL, string threadKey)
        {
            if (boardURL[boardURL.Length-1] == '/')
            {
                boardURL = boardURL.Remove(boardURL.Length-1);
            }
            _baseQueryURL = string.Concat(new string[]{
                boardURL,
                "/futaba.php?mode=json&res=",
                threadKey
            });
            _baseURL = boardURL;
            ThreadURL = string.Concat(new string[]{
                boardURL,
                "/res/",
                threadKey,
                ".htm"
            });
            _rng = new Random();
            _lastResNum = "";
            _checkThreadAvailabilityByRequest = false;
        }

        public async Task Run(int start, bool resume)
        {
            Running = true;
            if (resume && _pendingMessages != null)
            {
                FSSpeechClient.PushMessages(_pendingMessages);
                _pendingMessages = null;
                FSSpeechClient.UpdateStatus();
            }
            if (!resume)
            {
                _announcedThreadIsDying = false;
                _lastResNum = "";
                _pendingMessages = null;
                _lastCommittedMessageId = 0;
            }
            var status = await Task<Tuple<int, string>>.Run(() => {
                int sleepMilliseconds = 5000;
                int zeroCount = 0;
                int sleepCount = sleepMilliseconds;
                int exitStatus = 0;
                string errorMsg = null;
                int retry = 0;
                var messageFilter = FSSettings.SharedInstance().ContextProps.Filter;
                _contdownEvent.Reset();
                while (Running)
                {
                    FSFutabaJson json = null;
                    Int64 elapsed = DateTime.Now.Ticks;
                    int ret = GetFutabaJson(out json, out errorMsg);
                    elapsed = (DateTime.Now.Ticks - elapsed) / TimeSpan.TicksPerMillisecond;
                    if (json != null)
                    {
                        if (json.res != null && _lastResNum.Equals(""))
                        {
                            if (start > 1)
                            {
                                if (start > json.res.Count) start = json.res.Count;
                                var newDic = new Dictionary<string, FSFutabaJson.Res>();
                                int idx = 0;
                                foreach (var res in json.res)
                                {
                                    if (++idx >= start) newDic.Add(res.Key, res.Value);
                                }
                                json.res = newDic;
                            }
                            else if (start < 0)
                            {
                                int count = -start;
                                if (json.res.Count > count)
                                {
                                    var newDic = new Dictionary<string, FSFutabaJson.Res>();
                                    int idx = 0;
                                    count = json.res.Count - count;
                                    foreach (var res in json.res)
                                    {
                                        if (idx++ >= count) newDic.Add(res.Key, res.Value);
                                    }
                                    json.res = newDic;
                                }
                            }
                            else if (json.res.Count >= 20)
                            {
                                MessageBoxResult ret2 = Application.Current.Dispatcher.Invoke(() =>
                                {
                                    if (!Application.Current.MainWindow.IsActive) System.Media.SystemSounds.Beep.Play();
                                    return MessageBox.Show("20以上のレスを一度に読み上げようとしています。最新の10レス以降を読み上げますがよろしいですか？ (いいえを選ぶと全て読み上げます)", "読み上げ確認", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                });
                                if (ret2 == MessageBoxResult.Yes)
                                {
                                    var newDic = new Dictionary<string, FSFutabaJson.Res>();
                                    int idx = 0;
                                    int accept = json.res.Count - 10;
                                    foreach (var res in json.res)
                                    {
                                        if (idx++ >= accept) newDic.Add(res.Key, res.Value);
                                    }
                                    json.res = newDic;
                                }
                            }
                        }
                        AnalyzeFutabaJson(json, messageFilter);
                    }
                    if (ret == (int)FSFutabaConnectionError.Timeout)
                    {
                        if (retry++ == 5)
                        {
                            _pendingMessages = FSSpeechClient.GetPendingMessages();
                            exitStatus = ret;
                            break;
                        }
                    }
                    else if (ret < 0 && ret != (int)FSFutabaConnectionError.Timeout)
                    {
                        if (ret == (int)FSFutabaConnectionError.NotFound)
                        {
                            if (FSSettings.SharedInstance().ShouldAlertNotFound)
                            {
                                FSMessage message = new FSMessage(0, FSSettings.SharedInstance().NotFoundMessage, null);
                                FSSpeechClient.PushMessage(message);
                            }
                        }
                        else _pendingMessages = FSSpeechClient.GetPendingMessages();
                        exitStatus = ret;
                        break;
                    }
                    else if (ret == 0 && json != null && !string.IsNullOrEmpty(json.maxres))
                    {
                        if (FSSettings.SharedInstance().ShouldAlertThreadIsFull)
                        {
                            FSMessage message = new FSMessage(0, FSSettings.SharedInstance().ThreadIsFullMessage, null);
                            FSSpeechClient.PushMessage(message);
                        }
                        exitStatus = 1;
                        break;
                    }
                    else retry = 0;
                    if (ret <= 0)
                    {
                        if (sleepMilliseconds < 15000 && ++zeroCount == 3)
                        {
                            sleepMilliseconds += 5000;
                            zeroCount = 0;
                        }
                    }
                    else
                    {
                        sleepMilliseconds = 5000;
                        zeroCount = 0;
                    }
#if DEBUG
                    Console.WriteLine(ret + " new messages");
#endif
                    if (sleepMilliseconds - elapsed > 0)_contdownEvent.Wait(sleepMilliseconds - (int)elapsed);
                    _contdownEvent.Reset();
                }
                if (Running) Running = false;
                else _pendingMessages = FSSpeechClient.GetPendingMessages();
                return new Tuple<int, string>(exitStatus, errorMsg);
            });
            var mainWindow = Application.Current.MainWindow as MainWindow;
            mainWindow.TaskFinished(status.Item1, _lastResNum.Equals(""), status.Item2);
        }
        public void Pause()
        {
            Running = false;
            _contdownEvent.Signal();
        }
        private int GetFutabaJson(out FSFutabaJson json, out string errorMsg)
        {
            json = null;
            errorMsg = null;
            HttpWebResponse resp;
            try
            {
                if (_checkThreadAvailabilityByRequest)
                {
                    HttpWebRequest headReq = (HttpWebRequest)WebRequest.Create(ThreadURL);
                    headReq.Method = "HEAD";
                    headReq.Timeout = 5000;
                    ((HttpWebResponse)headReq.GetResponse()).Close();
                }
                HttpWebRequest jsonReq = (HttpWebRequest)WebRequest.Create(string.Concat(new string[]
                {
                    _baseQueryURL,
                    "&start=",
                    _lastResNum,
                    "&",
                    _rng.NextDouble().ToString("N16")
                }));
                jsonReq.Timeout = 5000;
                resp = (HttpWebResponse)jsonReq.GetResponse();
            }
            catch (WebException ex)
            {
                errorMsg = ex.Message;
                Console.WriteLine(ex.Message);
                HttpStatusCode statusCode = 0;
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    statusCode = ((HttpWebResponse)ex.Response).StatusCode;
                }
                if (ex.Status != WebExceptionStatus.Timeout && statusCode < HttpStatusCode.InternalServerError)
                {
                    if (statusCode == HttpStatusCode.NotFound)
                    {
                        return (int)FSFutabaConnectionError.NotFound;
                    }
                    return (int)FSFutabaConnectionError.Unknown;
                }
                return (int)FSFutabaConnectionError.Timeout;
            }

            StreamReader sr = new StreamReader(resp.GetResponseStream());
            FSFutabaJson futabaJson = null;
            try
            {
                futabaJson = JsonConvert.DeserializeObject<FSFutabaJson>(sr.ReadToEnd());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            resp.Close();

            json = futabaJson;
            if (!_checkThreadAvailabilityByRequest && futabaJson != null)
            {
                DateTime die = DateTime.ParseExact(futabaJson.dielong, "ddd, dd MMM yyyy HH:mm:ss GMT", CultureInfo.CreateSpecificCulture("en-US"));
                if (die.Year == 1970)
                {
                    json = null;
                    return (int)FSFutabaConnectionError.NotFound;
                }
            }
            if (futabaJson != null && futabaJson.res != null) return futabaJson.res.Count;
            return 0;
        }

        private void AnalyzeFutabaJson(FSFutabaJson futabaJson, FSMessageFilter messageFilter)
        {
            if (futabaJson == null) return;

            if (futabaJson.dielong != null)
            {
                if (!_announcedThreadIsDying)
                {
                    DateTime die;
                    if (FSSettings.SharedInstance().isDyingThread(futabaJson.old, futabaJson.dielong, out die))
                    {
                        double left = (double)(die.Ticks - DateTime.Now.Ticks) / TimeSpan.TicksPerMinute;
                        FSSpeechClient.SpeechText(FSSettings.SharedInstance().GetDyingAlertMessage((int)Math.Round(left)), 0, false);
                        _announcedThreadIsDying = true;
                    }
                }
            }
            if (futabaJson.res != null && futabaJson.res.Count > 0)
            {
                string quotePretext = FSSettings.SharedInstance().QuoteLinePretext;
                _checkThreadAvailabilityByRequest = true;
                foreach (var res in futabaJson.res)
                {
                    string resNum = res.Key;
                    string message = res.Value.com;
                    string messageWithoutAnchorTag = Regex.Replace(message, "<a href=[^<>]+>(?<text>[^<>]+)</a>", "${text}");
                    string messageWithoutFontTag = Regex.Replace(messageWithoutAnchorTag, "<font[^<>]*>(?<text>[^<>]+)</font>", "${text}");
                    string[] lines = messageWithoutFontTag.Split(new string[]{"<br>"}, StringSplitOptions.None);
                    bool isDeletedMessage = res.Value.del != null && res.Value.del != "";
                    bool hasImage = !isDeletedMessage && res.Value.w != 0 && res.Value.h != 0 && res.Value.fsize != 0;
                    if (isDeletedMessage && FSSettings.SharedInstance().SkipDeletedMessages) goto last;
                    if (!messageFilter.TestFutabaPost(res.Value, messageWithoutFontTag)) goto last;
                    string futabaUpImageURL = null;
                    if (!hasImage)
                    {
                        for (int i = 0; i < lines.Length; i++)
                        {
                            if (!lines[i].StartsWith("&gt;"))
                            {
                                Match match = Regex.Match(lines[i], "fu?[0-9]+\\.(?:jpg|jpeg|png|gif)");
                                if (match.Success)
                                {
                                    futabaUpImageURL = "https://dec.2chan.net/";
                                    if (match.Value.StartsWith("fu")) futabaUpImageURL += "up2/src/";
                                    else futabaUpImageURL += "up/src/";
                                    futabaUpImageURL += match.Value;
                                    break;
                                }
                            }
                        }
                    }
                    
                    string pretext = FSSettings.SharedInstance().GetPretext(res.Value.rsc, hasImage || (futabaUpImageURL != null));
                    FSMessage group = new FSMessage(res.Value.rsc, pretext, FSSettings.SharedInstance().TrimQuoteMark ? quotePretext : null);
                    for (int i = 0; i < lines.Length; i++)
                    {
                        group.AddLine(lines[i]);
                    }
                    if (hasImage)
                    {
                        string mediaURL = _baseURL + res.Value.src;
                        string type = Path.GetExtension(res.Value.src).ToLower();
                        group.OriginalMediaURL = mediaURL;
                        if (type != ".jpg" && type != ".png" && type != ".gif")
                        {
                            mediaURL = _baseURL + res.Value.thumb;
                            group.UnknownMediaType = type;
                        }
                        group.MediaURL = mediaURL;
                        group.MediaDownloadTask = FSDownloadTaskManager.CreateTaskForURL(mediaURL);
                    }
                    else if (futabaUpImageURL != null)
                    {
                        group.OriginalMediaURL = futabaUpImageURL;
                        group.MediaURL = futabaUpImageURL;
                        group.MediaDownloadTask = FSDownloadTaskManager.CreateTaskForURL(futabaUpImageURL);
                    }
                    FSSpeechClient.PushMessage(group);
                last:
                    _lastResNum = (int.Parse(resNum) + 1).ToString();
                    _lastCommittedMessageId = res.Value.rsc;
                    if (!Running) break;
                }
                FSSpeechClient.UpdateStatus();
            }
        }
    }
}
