﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace FutabaSpeech
{
    /// <summary>
    /// FSCommentWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class FSCommentWindow : Window
    {
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        private double _fontSize = FSSettings.SharedInstance().ContextProps.CommentTextSize;
        private FontFamily _fontFamily;
        private int _numLines = FSSettings.SharedInstance().ContextProps.CommentLines;
        private double _margin;
        private double _spacing = FSSettings.SharedInstance().ContextProps.CommentLineSpacing;
        private const double _bottomMargin = 1.0;
        private LinkedList<OutlineText> _textQueue = new LinkedList<OutlineText>();
        private bool _animating;
        private double _bulletWidth = 1.0;
        private char _bullet = '＊';
        private bool _shouldRedraw;
        private Timer _vanishTimer;
        private Brush _textColor;
        private Brush _quoteTextColor;
        private Brush _outlineColor;
        private Brush _backgroundColor;
        private double canvasHeightToWindowHeight;
        private bool _useBoldFont = FSSettings.SharedInstance().ContextProps.CommentUseBoldFont;
        private double _outlineThicknessRatio = FSSettings.SharedInstance().ContextProps.OutlineThickness;
        public List<FontFamily> FontList { get; private set; }
        public bool IsClosed
        {
            get
            {
                return (PresentationSource.FromVisual(this) == null || this.Visibility != Visibility.Visible);
            }
        }
        public double Spacing
        {
            get
            {
                return _spacing;
            }
            set
            {
                if (!_animating)
                {
                    FSSettings.SharedInstance().ContextProps.CommentLineSpacing = value;
                    _spacing = value;
                    Redraw();
                }
            }
        }
        public double TextSize
        {
            get
            {
                return _fontSize;
            }
            set
            {
                if (!_animating)
                {
                    double oldSize = _fontSize;
                    FSSettings.SharedInstance().ContextProps.CommentTextSize = value;
                    _fontSize = value;
                    _margin = _fontSize * (1 - _fontFamily.Baseline);
                    Redraw();
                    this.Width *= _fontSize / oldSize;
                }
            }
        }
        public FontFamily TextFont
        {
            get
            {
                return _fontFamily;
            }
            set
            {
                FSSettings.SharedInstance().ContextProps.CommentTextFont = value.Source;
                _fontFamily = value;
                _margin = _fontSize * (1 - _fontFamily.Baseline);
                //UpdateBulletWidth();
                if (!_animating)
                {
                    Redraw();
                }
                else
                {
                    _shouldRedraw = true;
                }
            }
        }
        public int NumLines
        {
            get
            {
                return _numLines;
            }
            set
            {
                if (!_animating)
                {
                    FSSettings.SharedInstance().ContextProps.CommentLines = value;
                    _numLines = value;
                    ResizeToFit();
                    if (_textQueue.Count > _numLines)
                    {
                        FeedLinesAndPush(true);
                    }
                }
            }
        }
        public char Bullet {
            get
            {
                return _bullet;
            }
            set
            {
                _bullet = value;
                //UpdateBulletWidth();
            }
        }
        public Brush TextColor
        {
            get
            {
                return _textColor;
            }
            set
            {
                FSSettings.SharedInstance().ContextProps.CommentTextColor = (value as SolidColorBrush).Color.ToString();
                _textColor = value;
                if (!_animating)
                {
                    Redraw();
                }
                else
                {
                    _shouldRedraw = true;
                }

            }
        }
        public Brush QuoteTextColor
        {
            get
            {
                return _quoteTextColor;
            }
            set
            {
                FSSettings.SharedInstance().ContextProps.CommentQuoteTextColor = (value as SolidColorBrush).Color.ToString();
                _quoteTextColor = value;
                if (!_animating)
                {
                    Redraw();
                }
                else
                {
                    _shouldRedraw = true;
                }

            }
        }
        public Brush OutlineColor
        {
            get
            {
                return _outlineColor;
            }
            set
            {
                FSSettings.SharedInstance().ContextProps.CommentOutlineColor = (value as SolidColorBrush).Color.ToString();
                _outlineColor = value;
                if (!_animating)
                {
                    Redraw();
                }
                else
                {
                    _shouldRedraw = true;
                }

            }
        }
        public Brush BackgroundColor
        {
            get
            {
                return _backgroundColor;
            }
            set
            {
                FSSettings.SharedInstance().ContextProps.CommentBackgroundColor = (value as SolidColorBrush).Color.ToString();
                _backgroundColor = value;
                this.Background = _backgroundColor;
            }
        }
        public double OutlineThickness
        {
            get
            {
                return _outlineThicknessRatio;
            }
            set
            {
                FSSettings.SharedInstance().ContextProps.OutlineThickness = value;
                _outlineThicknessRatio = value;
                if (!_animating)
                {
                    Redraw();
                }
                else
                {
                    _shouldRedraw = true;
                }
            }
        }
        public ObservableCollection<FSCommentColor> TextColors { get; set; }
        public ObservableCollection<FSCommentColor> QuoteTextColors { get; set; }
        public ObservableCollection<FSCommentColor> OutlineColors { get; set; }
        public ObservableCollection<FSCommentColor> BackgroundColors { get; set; }
        public bool UseBoldFont {
            get
            {
                return _useBoldFont;
            }
            set
            {
                FSSettings.SharedInstance().ContextProps.CommentUseBoldFont = value;
                _useBoldFont = value;
                if (!_animating)
                {
                    Redraw();
                }
                else
                {
                    _shouldRedraw = true;
                }
            }
        }
        public FSCommentWindow()
        {
            InitializeComponent();
            _fontFamily = BuildFontList(FSSettings.SharedInstance().ContextProps.CommentTextFont);
            TextColors = BuildColorList(false, FSSettings.SharedInstance().ContextProps.CommentTextColor, out _textColor);
            QuoteTextColors = BuildColorList(false, FSSettings.SharedInstance().ContextProps.CommentQuoteTextColor, out _quoteTextColor);
            OutlineColors = BuildColorList(true, FSSettings.SharedInstance().ContextProps.CommentOutlineColor, out _outlineColor);
            BackgroundColors = BuildColorList(false, FSSettings.SharedInstance().ContextProps.CommentBackgroundColor, out _backgroundColor); ;
            this.WindowStyle = WindowStyle.None;
            this.Background = _backgroundColor;
            this.SizeToContent = SizeToContent.Height;
            _margin = _fontSize * (1 - _fontFamily.Baseline);
            double height = _bottomMargin + _fontSize * (_numLines + _fontFamily.LineSpacing - _fontFamily.Baseline) + _spacing * (_numLines - 1);
            if (_margin > 0) height += _margin;
            this.CommentCanvas.Height = Math.Ceiling(height);
            //UpdateBulletWidth();
        }
        public FSCommentWindow(int numLines, FontFamily family, double fontSize)
        {
            InitializeComponent();
            _numLines = numLines;
            _fontFamily = BuildFontList(family.Source);
            _fontSize = fontSize;
            TextColors = BuildColorList(false, "#ffffff", out _textColor);
            QuoteTextColors = BuildColorList(false, "#d3d3d3", out _quoteTextColor);
            OutlineColors = BuildColorList(true, "#000000", out _outlineColor);
            BackgroundColors = BuildColorList(false, "#ff00ff", out _backgroundColor);
            this.WindowStyle = WindowStyle.None;
            //this.AllowsTransparency = true;
            this.Background = _backgroundColor;
            this.SizeToContent = SizeToContent.Height;
            _margin = _fontSize * (1 - _fontFamily.Baseline);
            double height = _bottomMargin + _fontSize * (_numLines + _fontFamily.LineSpacing - _fontFamily.Baseline) + _spacing * (_numLines - 1);
            if (_margin > 0) height += _margin;
            this.CommentCanvas.Height = Math.Ceiling(height);
            //UpdateBulletWidth();
        }
        public void PutLine(string text, int bulletType, bool isQuoted)
        {
            if (IsClosed) return;
            var ot = new OutlineText();
            ot.Text = text;
            ot.FontSize = _fontSize;
            ot.IsQuotedText = isQuoted;
            if (isQuoted) ot.Foreground = _quoteTextColor;
            else ot.Foreground = _textColor;
            ot.Outline = _outlineColor;
            if (_fontFamily != null) ot.FontFamily = _fontFamily;
            if (_useBoldFont) ot.FontWeight = FontWeights.Bold;
            else ot.FontWeight = FontWeights.Normal;
            ot.OutlineThickness = ot.FontSize * _outlineThicknessRatio;
            if (bulletType == 0) ot.Margin = new Thickness(_fontSize * _bulletWidth, 0, 0, 0);
            else
            {
                ot.BulletType = bulletType;
                ot.Padding = new Thickness(_fontSize+5, 0, 5, 0);
            }
            ot.TextTrimming = TextTrimming.CharacterEllipsis;
            Binding binding = new Binding("ActualWidth");
            binding.Source = CommentCanvas;
            binding.Mode = BindingMode.OneWay;
            ot.SetBinding(OutlineText.WidthProperty, binding);
            TextOptions.SetTextFormattingMode(ot, TextFormattingMode.Ideal);
            if (_fontSize <= 20) TextOptions.SetTextHintingMode(ot, TextHintingMode.Animated);
            _textQueue.AddLast(ot);
            if (!_animating) FeedLinesAndPush(false);
        }
        private void PopLine(EventHandler handler)
        {
            var sb = new Storyboard();
            int idx = 0;
            foreach (var text in _textQueue)
            {
                if (!CommentCanvas.Children.Contains(text)) break;
                var a = new DoubleAnimation();
                Storyboard.SetTarget(a, text);
                Storyboard.SetTargetProperty(a, new PropertyPath("(Canvas.Top)"));
                double diff = (idx++ == 0) ? _fontSize + _margin : _fontSize + _spacing;
                double to = Canvas.GetTop(text) - diff;
                a.To = to;
                a.Duration = TimeSpan.FromSeconds(0.2);
                a.FillBehavior = FillBehavior.Stop;
                sb.Children.Add(a);
            }
            sb.Completed += new EventHandler((object sender, EventArgs e) =>
            {
                int idx2 = -1;
                foreach (UIElement elem in CommentCanvas.Children)
                {
                    if (idx2 >= 0)
                    {
                        double y = idx2 * _fontSize + _margin + (idx2 > 0 ? _spacing * idx2 : 0);
                        Canvas.SetTop(elem, y);
                    }
                    else Canvas.SetTop(elem, -1 * (_fontSize + _margin));
                    idx2++;
                }
                CommentCanvas.Children.Remove(_textQueue.First.Value);
                _textQueue.RemoveFirst();
            });
            if (handler != null) sb.Completed += handler;
            sb.Begin();
        }
        private void PopAllLines()
        {
            var sb = new Storyboard();
            double diff = CommentCanvas.Height / _numLines * CommentCanvas.Children.Count;
            foreach (var text in _textQueue)
            {
                if (!CommentCanvas.Children.Contains(text)) break;
                var a = new DoubleAnimation();
                Storyboard.SetTarget(a, text);
                Storyboard.SetTargetProperty(a, new PropertyPath("(Canvas.Top)"));
                double to = Canvas.GetTop(text) - diff;
                a.To = to;
                a.Duration = TimeSpan.FromSeconds(0.5 / _numLines * CommentCanvas.Children.Count);
                a.FillBehavior = FillBehavior.Stop;
                sb.Children.Add(a);
            }
            _textQueue.Clear();
            sb.Completed += new EventHandler((object sender, EventArgs e) =>
            {
                this.CommentCanvas.Children.Clear();
                if (_textQueue.Count != 0) FeedLinesAndPush(false);
                else _animating = false;
            });
            sb.Begin();
        }
        private void PushLine(OutlineText text)
        {
            var sb = new Storyboard();
            var a = new DoubleAnimation();
            int idx = this.CommentCanvas.Children.Count;
            double y = idx * _fontSize + _margin + (idx > 0 ? _spacing * idx : 0);
            Canvas.SetLeft(text, CommentCanvas.ActualWidth);
            if (!CommentCanvas.Children.Contains(text))
            {
                Canvas.SetTop(text, y);
                CommentCanvas.Children.Add(text);
            }
            Storyboard.SetTarget(a, text);
            Storyboard.SetTargetProperty(a, new PropertyPath("(Canvas.Left)"));
            a.To = 0;
            a.Duration = TimeSpan.FromSeconds(0.2);
            a.FillBehavior = FillBehavior.Stop;
            sb.Children.Add(a);
            sb.Completed += new EventHandler((object sender, EventArgs e) =>
            {
                Canvas.SetLeft(CommentCanvas.Children[CommentCanvas.Children.Count - 1], 0);
                if (_textQueue.Count > this.CommentCanvas.Children.Count) FeedLinesAndPush(false);
                else
                {
                    if (_shouldRedraw) Redraw();
                    _animating = false;
                    double timeout = FSSettings.SharedInstance().ContextProps.CommentViewTimeout * 1000;
                    if (timeout < 10000) timeout = 10000;
                    if (_vanishTimer == null || !_vanishTimer.Enabled)
                    {
                        if (_vanishTimer == null)
                        {
                            _vanishTimer = new Timer();
                            _vanishTimer.AutoReset = false;
                            _vanishTimer.Elapsed += (timer, ev) =>
                            {
                                this.Dispatcher.BeginInvoke(DispatcherPriority.Background, (Action)Vanish);
                            };
                        }
                        _vanishTimer.Interval = timeout;
                        _vanishTimer.Start();
                    }
                    else
                    {
                        _vanishTimer.Interval = timeout;
                    }
                }
            });
            sb.Begin();
        }
        private void FeedLinesAndPush(bool feedOnly)
        {
            if (feedOnly)
            {
                if (_textQueue.Count > _numLines)
                {
                    _animating = true;
                    PopLine(new EventHandler((object sender, EventArgs e) =>
                    {
                        if (_textQueue.Count > _numLines) FeedLinesAndPush(true);
                        else _animating = false;
                    }));
                }
            }
            else
            {
                _animating = true;
                var node = _textQueue.First;
                while (node != null && node.Next != null && this.CommentCanvas.Children.Contains(node.Value))
                {
                    node = node.Next;
                }
                var ot = node.Value;
                if (_textQueue.Count > _numLines)
                {
                    PopLine(new EventHandler((object sender, EventArgs e) =>
                    {
                        PushLine(ot);
                    }));
                }
                else
                {
                    PushLine(ot);
                }
            }
        }
        private void ResizeToFit()
        {
            if (PresentationSource.FromVisual(this) == null) return;
            double newHeight = _bottomMargin + _fontSize * (_numLines + _fontFamily.LineSpacing - _fontFamily.Baseline) + _spacing * (_numLines - 1);
            if (_margin > 0) newHeight += _margin;
            this.CommentCanvas.Height = Math.Ceiling(newHeight);
            this.MaxHeight = Double.PositiveInfinity;
            this.MinHeight = 0;
            this.Height = newHeight + canvasHeightToWindowHeight;
            this.MaxHeight = this.Height;
            this.MinHeight = this.Height;
        }
        private void Redraw()
        {
            _shouldRedraw = false;
            int idx = 0;
            //this.CommentCanvas.Children.RemoveRange(0, this.CommentCanvas.Children.Count - 1);
            foreach (var text in _textQueue)
            {
                text.FontSize = _fontSize;
                text.OutlineThickness = _fontSize * _outlineThicknessRatio;
                text.FontFamily = _fontFamily;
                if (text.IsQuotedText) text.Foreground = _quoteTextColor;
                else text.Foreground = _textColor;
                text.Outline = _outlineColor;
                if (_fontSize <= 20) TextOptions.SetTextHintingMode(text, TextHintingMode.Animated);
                else text.SetValue(TextOptions.TextHintingModeProperty, DependencyProperty.UnsetValue);
                if (_useBoldFont) text.FontWeight = FontWeights.Bold;
                else text.FontWeight = FontWeights.Normal;
                if (text.Margin.Left != 0) text.Margin = new Thickness(_fontSize * _bulletWidth, 0, 0, 0);
                else
                {
                    text.Padding = new Thickness(_fontSize+5, 0, 5, 0);
                }
                if (this.CommentCanvas.Children.Contains(text))
                {
                    double y = idx * _fontSize + _margin + (idx > 0 ? _spacing * idx : 0);
                    Canvas.SetTop(text, y);
                }
                idx++;
            }
            ResizeToFit();
        }
        private void UpdateBulletWidth()
        {
            var typefaces = _fontFamily.GetTypefaces();
            foreach (Typeface typeface in typefaces)
            {
                GlyphTypeface glyph;
                ushort glyphIndex;
                typeface.TryGetGlyphTypeface(out glyph);
                if (glyph != null && glyph.CharacterToGlyphMap.TryGetValue(Convert.ToUInt16('＊'), out glyphIndex))
                {
                    _bulletWidth = glyph.AdvanceWidths[glyphIndex];
                    break;
                }
            }
        }
        private void Vanish()
        {
            if (_animating) return;
            _animating = true;
            PopAllLines();
            
        }
        private FontFamily BuildFontList(string preferred)
        {
            var list = Fonts.SystemFontFamilies;
            var nonJapaneseList = new List<FontFamily>();
            var japaneseList = new List<FontFamily>();
            int unicodeValue = Convert.ToUInt16('あ');
            FontFamily defaultFont = null;
            foreach (var family in list)
            {
                if (family.Source == preferred) defaultFont = family;
                bool added = false;
                var typefaces = family.GetTypefaces();
                foreach (Typeface typeface in typefaces)
                {
                    GlyphTypeface glyph;
                    ushort glyphIndex;
                    typeface.TryGetGlyphTypeface(out glyph);
                    if (glyph != null && glyph.CharacterToGlyphMap.TryGetValue(unicodeValue, out glyphIndex))
                    {
                        japaneseList.Add(family);
                        added = true;
                        break;
                    }
                }
                if (!added) nonJapaneseList.Add(family);
            }
            japaneseList.AddRange(nonJapaneseList);
            FontList = japaneseList;
            if (defaultFont != null) return defaultFont;
            return japaneseList[0];
        }
        private ObservableCollection<FSCommentColor> BuildColorList(bool includeTransparent, string preferred, out Brush defaultColor)
        {
            defaultColor = null;
            var list = new ObservableCollection<FSCommentColor>
            {
                new FSCommentColor(){ Name="ブラック", Color=Brushes.Black },
                new FSCommentColor(){ Name="ホワイト", Color=Brushes.White },
                new FSCommentColor(){ Name="グレー", Color=Brushes.Silver },
                new FSCommentColor(){ Name="ブラウン", Color=Brushes.Brown },
                new FSCommentColor(){ Name="ブルー", Color=Brushes.Blue },
                new FSCommentColor(){ Name="シアン", Color=Brushes.Cyan },
                new FSCommentColor(){ Name="グリーン", Color=Brushes.Lime },
                new FSCommentColor(){ Name="マゼンタ", Color=Brushes.Magenta },
                new FSCommentColor(){ Name="オレンジ", Color=Brushes.Orange },
                new FSCommentColor(){ Name="パープル", Color=Brushes.Purple },
                new FSCommentColor(){ Name="レッド", Color=Brushes.Red },
                new FSCommentColor(){ Name="イエロー", Color=Brushes.Yellow },
                new FSCommentColor(){ Name="その他", Color=Brushes.White },
            };
            if (includeTransparent) list.Insert(0, new FSCommentColor() { Name = "なし", Color = Brushes.Transparent });
            var brush = (SolidColorBrush)(new BrushConverter().ConvertFrom(preferred));
            bool match = false;
            foreach (var color in list)
            {
                var currentBrush = color.Color as SolidColorBrush;
                if (currentBrush.Color.Equals(brush.Color))
                {
                    defaultColor = color.Color;
                    match = true;
                    break;
                }
            }
            if (!match)
            {
                defaultColor = brush;
                list.Last().Color = brush;
            }
            return list;
        }
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            PostMessage(hwnd, 0xA1, (IntPtr)2, IntPtr.Zero);
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            this.Height = this.ActualHeight;
            this.MaxHeight = this.ActualHeight;
            this.MinHeight = this.ActualHeight;
            this.SizeToContent = SizeToContent.Manual;
            canvasHeightToWindowHeight = this.Height - this.CommentCanvas.Height;
            //this.CommentCanvas.Height = Double.NaN;
        }
    }
    public class FontFamilyToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var v = value as FontFamily;
            var currentLang = XmlLanguage.GetLanguage(culture.IetfLanguageTag);
            return v.FamilyNames.FirstOrDefault(o => o.Key == currentLang).Value ?? v.Source;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class FSCommentColor
    {
        public string Name { get; set; }
        public Brush Color { get; set; }
    }
}
