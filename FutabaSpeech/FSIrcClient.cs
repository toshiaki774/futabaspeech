﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FutabaSpeech
{
    public class FSIrcClient
    {
        private const string _defaultServer = "irc.chat.twitch.tv";
        private const int _defaultPort = 6667;
        private string _nickName;
        private string _password;
        private TcpClient _client;
        private StringBuilder _commandBuffer = new StringBuilder();
        private List<string> _channels = new List<string>();
        private Socket _socket;
        private int _messageId = 100000;
        private MainWindow _mainWindow;
        public bool Running;
        public bool Connected
        {
            get
            {
                return _client != null && _socket.Connected;
            }
        }
        public bool Connect(string server, int port)
        {
            if (_client != null) Disconnect();
            try
            {
                _client = new TcpClient(server, port);
            }
            catch (SocketException ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            _socket = _client.Client;
            return true;
        }

        public bool Connect()
        {
            return this.Connect(_defaultServer, _defaultPort);
        }

        public void Login()
        {
            Byte[] cmd = Encoding.UTF8.GetBytes(String.Format("PASS {0}\r\nNICK {1}\r\n", _password, _nickName));
            _socket.Send(cmd);
        }

        public void Join(string channel)
        {
            if (_channels.Contains(channel)) return;
            lock (_commandBuffer)
            {
                _commandBuffer.AppendFormat("JOIN {0}\r\n", channel);
            }
        }

        public void Part(string channel)
        {
            lock (_commandBuffer)
            {
                _commandBuffer.AppendFormat("PART {0}\r\n", channel);
            }
        }

        public void Part()
        {
            if (_channels.Count == 0) return;
            lock (_commandBuffer)
            {
                _commandBuffer.AppendFormat("PART {0}\r\n", _channels[0]);
            }
        }

        public void SendCommand(string command)
        {
            lock (_commandBuffer)
            {
                _commandBuffer.AppendFormat("{0}\r\n", command);
            }
        }
        public async Task Run()
        {
            Running = true;
            await Task.Run(() =>
            {
                byte[] recvBuffer = new byte[4096];
                int sleepSecond = 1;
                int connectionStatus = 0;
                var messageFilter = FSSettings.SharedInstance().ContextProps.Filter;
                while (Running)
                {
                    connectionStatus = 0;
                    if (!Connected && this.Connect() == false)
                    {
                        Thread.Sleep(sleepSecond * 1000);
                        if (sleepSecond < 64) sleepSecond <<= 1;
                        continue;
                    }
                    sleepSecond = 1;
                    int pos = 0;
                    List<Socket> readSockets = new List<Socket>();
                    List<Socket> writeSockets = new List<Socket>();
                    while (Running)
                    {
                        readSockets.Add(_socket);
                        if (connectionStatus == 0) writeSockets.Add(_socket);
                        else if (connectionStatus == 2 && _commandBuffer.Length > 0) writeSockets.Add(_socket);
                        try
                        {
                            Socket.Select(readSockets, writeSockets, null, 500000);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("***Select error: {0}", ex.Message);
                            break;
                        }
                        if (readSockets.Count > 0)
                        {
                            if (pos >= 4096) pos = 0;
                            int ret = 0;
                            try
                            {
                                ret = _socket.Receive(recvBuffer, pos, 4096 - pos, SocketFlags.None);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("***Receive error: {0}", ex.Message);
                                break;
                            }
                            //Console.WriteLine(Encoding.UTF8.GetString(recvBuffer, 0, ret));
                            pos = 0;
                            while (pos < ret)
                            {
                                try
                                {
                                    int tmp = pos;
                                    while (tmp < ret - 1)
                                    {
                                        if (recvBuffer[tmp] == '\r' && recvBuffer[tmp + 1] == '\n') break;
                                        tmp++;
                                    }
                                    if (tmp == ret - 1) break;
                                    int end = tmp;
                                    int start;
                                    string name = "";
                                    string command = "";
                                    string param = "";
                                    string data = "";
                                    if (recvBuffer[pos] == ':')
                                    {
                                        start = pos + 1;
                                        while (pos < end)
                                        {
                                            if (recvBuffer[pos++] == ' ') break;
                                        }
                                        if (pos == end) break;
                                        name = Encoding.UTF8.GetString(recvBuffer, start, pos - 1 - start);
                                    }
                                    start = pos;
                                    while (pos < end)
                                    {
                                        if (recvBuffer[pos++] == ' ') break;
                                    }
                                    if (pos == end) break;
                                    command = Encoding.UTF8.GetString(recvBuffer, start, pos - 1 - start);
                                    start = pos;
                                    while (pos < end)
                                    {
                                        if (recvBuffer[pos++] == ':') break;
                                    }
                                    if (pos < end)
                                    {
                                        data = Encoding.UTF8.GetString(recvBuffer, pos, end - pos);
                                        pos -= 2;
                                    }
                                    if (pos > start) param = Encoding.UTF8.GetString(recvBuffer, start, pos - start);
                                    //Console.WriteLine("{0},{1},{2},{3}|",name,command,param,data);
                                    bool echoBack = true;
                                    if (command == "PING")
                                    {
                                        lock (_commandBuffer)
                                        {
                                            _commandBuffer.AppendFormat("PONG :{0}\r\n", data);
                                        }
                                    }
                                    else if (command == "PRIVMSG")
                                    {
                                        echoBack = false;
                                        if (FSSettings.SharedInstance().TwitchReadingMode == 0 || !_mainWindow.HasActiveThread())
                                        {
                                            string poster = name.Split('!')[0];
                                            if (messageFilter.TestTwitchChat(poster, data))
                                            {
                                                var message = new FSMessage(_messageId++, null, null);
                                                message.AddLine(data);
                                                FSSpeechClient.PushMessage(message);
                                                FSSpeechClient.UpdateStatus();
                                            }
                                        }
#if DEBUG
                                        string shortName = name.Split('!')[0];
                                        Console.WriteLine("{0}: {1}", shortName, data);
#endif
                                    }
                                    else if (command == "JOIN")
                                    {
                                        lock (_channels)
                                        {
                                            _channels.Insert(0, param);
                                        }
                                    }
                                    else if (command == "PART")
                                    {
                                        lock (_channels)
                                        {
                                            _channels.Remove(param);
                                        }
                                    }
                                    else if (connectionStatus == 1 && (command == "376" || command == "422"))
                                    {
                                        connectionStatus = 2;
                                        if (_channels.Count > 0)
                                        {
                                            lock (_commandBuffer)
                                            {
                                                foreach (string channel in _channels)
                                                {
                                                    _commandBuffer.AppendFormat("JOIN {0}\r\n", channel);
                                                }
                                            }
                                            _channels.Clear();
                                        }
                                    }
#if DEBUG
                                    if (echoBack)
                                    {
                                        Console.WriteLine("{0} - {1} {2}", command, param, data);
                                    }
#endif
                                    pos = end + 2;
                                }
                                catch (Exception ex)
                                {
                                    pos = ret;
                                    break;
                                }
                            }
                            //Console.WriteLine("{0} bytes left",ret-pos);
                            if (pos > 0 && pos < ret)
                            {
                                Buffer.BlockCopy(recvBuffer, pos, recvBuffer, 0, ret - pos);
                            }
                            pos = ret - pos;
                        }
                        if (writeSockets.Count > 0)
                        {
                            if (connectionStatus == 0)
                            {
                                try
                                {
                                    Login();
                                    connectionStatus = 1;
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message);
                                    break;
                                }
                            }
                            else
                            {
                                lock (_commandBuffer)
                                {
                                    if (_commandBuffer.Length > 0)
                                    {
                                        try
                                        {
                                            _socket.Send(Encoding.UTF8.GetBytes(_commandBuffer.ToString()));
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine(ex.Message);
                                            break;
                                        }
                                        _commandBuffer.Length = 0;
                                    }
                                }
                            }
                        }
                        readSockets.Clear();
                        writeSockets.Clear();
                        //Console.WriteLine("timeout, repeating");
                    }
                    Disconnect();
                    if (connectionStatus == 2)
                    {
                        lock (_commandBuffer)
                        {
                            _commandBuffer.Length = 0;
                        }
                    }
                }
            });
            _channels.Clear();
            _mainWindow.Check1.IsEnabled = true;
        }
        public void Disconnect()
        {
            _client.Close();
            _socket = null;
            _client = null;
        }

        public FSIrcClient(string nick, string pass, MainWindow window)
        {
            _mainWindow = window;
            _nickName = nick;
            _password = pass;
        }
        public FSIrcClient(MainWindow window)
        {
            _mainWindow = window;
            Random random = new Random();
            _nickName = "justinfan" + (1000 + random.Next(80000)).ToString();
            _password = "SCHMOOPIIE";
        }
    }
}
