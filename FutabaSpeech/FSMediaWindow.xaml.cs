﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace FutabaSpeech
{
    /// <summary>
    /// FSMediaWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class FSMediaWindow : Window
    {
        [DllImport("user32.dll")]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        private static Geometry LOADING_ICON = Geometry.Parse("M6,2V8H6V8L10,12L6,16V16H6V22H18V16H18V16L14,12L18,8V8H18V2H6M16,16.5V20H8V16.5L12,12.5L16,16.5M12,11.5L8,7.5V4H16V7.5L12,11.5Z");
        private static Geometry ERROR_ICON = Geometry.Parse("M21,5V11.59L18,8.58L14,12.59L10,8.59L6,12.59L3,9.58V5A2,2 0 0,1 5,3H19A2,2 0 0,1 21,5M18,11.42L21,14.43V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V12.42L6,15.41L10,11.41L14,15.41");
        private double _aspectRatio = 1;
        private double _horizontalMargin;
        private double _verticalMargin;
        private MemoryStream _pendingData;
        private string _pendingText;
        private BitmapSource _altSource;
        public int CurrentMessageId { get; set; }
        public string OriginalMediaURL { get; set; }
        public bool IsClosed
        {
            get
            {
                return (PresentationSource.FromVisual(this) == null || this.Visibility != Visibility.Visible);
            }
        }
        public FSMediaWindow()
        {
            InitializeComponent();
            this.WindowStyle = WindowStyle.None;
            RenderOptions.SetBitmapScalingMode(ImageView, BitmapScalingMode.Fant);
            this.BadgeText.FontSize = 24;
            this.BadgeText.OutlineThickness = 2.4;
            this.BadgeText.Foreground = Brushes.White;
            this.BadgeText.Outline = Brushes.Black;
            this.BadgeText.Opacity = 0.8;
        }
        public void ShowImageFromStream(MemoryStream stream, string badgeText)
        {
            if (stream == null)
            {
                if (!IsClosed)
                {
                    this.ImageView.Source = null;
                    _altSource = null;
                    this.PlaceholderIcon.Visibility = Visibility.Visible;
                    this.BadgeText.Visibility = Visibility.Hidden;
                    this.PlaceholderIcon.Data = ERROR_ICON;
                }
                return;
            }
            if (IsClosed)
            {
                _pendingData = stream;
                _pendingText = badgeText;
                return;
            }
            _altSource = null;
            _pendingData = null;
            _pendingText = null;
            this.PlaceholderIcon.Visibility = Visibility.Hidden;
            if (badgeText != null)
            {
                this.BadgeText.Visibility = Visibility.Visible;
                this.BadgeText.Text = badgeText.TrimStart('.');
            }
            else this.BadgeText.Visibility = Visibility.Hidden;
            try
            {
                BitmapDecoder decoder = BitmapDecoder.Create(
                    stream,
                    BitmapCreateOptions.None,
                    BitmapCacheOption.OnLoad);
                if (decoder.GetType() == typeof(GifBitmapDecoder) && decoder.Frames.Count > 1)
                {
                    this.BadgeText.Visibility = Visibility.Visible;
                    this.BadgeText.Text = "AGIF";
                }
                var bitmap = decoder.Frames[0];
                /*var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.StreamSource = stream;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.EndInit();
                bitmap.Freeze();*/
                _aspectRatio = (double)bitmap.PixelWidth / bitmap.PixelHeight;
                double baseSize = Math.Max(this.ActualWidth, this.ActualHeight);
                if (bitmap.PixelWidth > bitmap.PixelHeight)
                {
                    this.Width = baseSize;
                    this.Height = (baseSize - _horizontalMargin) / _aspectRatio + _verticalMargin;
                }
                else
                {
                    this.Width = (baseSize - _verticalMargin) * _aspectRatio + _horizontalMargin;
                    this.Height = baseSize;
                }
                if (FSSettings.SharedInstance().ContextProps.ApplyMosaicAutomatically)
                {
                    ImageView.Source = ApplyMosaic(bitmap, 16);
                    _altSource = bitmap;
                }
                else ImageView.Source = bitmap;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void ShowPendingImage()
        {
            if (_pendingData != null)
            {
                ShowImageFromStream(_pendingData, _pendingText);
            }
        }
        public void ShowLoadingIcon()
        {
            if (IsClosed) return;
            this.ImageView.Source = null;
            _altSource = null;
            this.PlaceholderIcon.Visibility = Visibility.Visible;
            this.BadgeText.Visibility = Visibility.Hidden;
            this.PlaceholderIcon.Data = LOADING_ICON;
        }
        private BitmapSource ApplyMosaic(BitmapSource source, int granularity)
        {
            var original = source;
            if (original.PixelWidth > this.ActualWidth - _horizontalMargin)
            {
                double ratio = (this.ActualWidth - _horizontalMargin) / original.PixelWidth;
                original = new WriteableBitmap(new TransformedBitmap(original, new ScaleTransform(ratio, ratio)));
            }
            if (original.Format != PixelFormats.Bgra32)
            {
                original = new FormatConvertedBitmap(original, PixelFormats.Bgra32, null, 0);
                original.Freeze();
            }
            int stride = original.PixelWidth * 4;
            byte[] pixels = new byte[original.PixelHeight * stride];
            original.CopyPixels(pixels, stride, 0);
            for (int i = 0; i < original.PixelHeight; i += granularity)
            {
                uint heightToSum = (uint)((i + granularity > original.PixelHeight) ? original.PixelHeight - i : granularity);
                for (int j = 0; j < original.PixelWidth; j += granularity)
                {
                    uint sumB = 0;
                    uint sumG = 0;
                    uint sumR = 0;
                    uint sumA = 0;
                    uint widthToSum = (uint)((j + granularity > original.PixelWidth) ? original.PixelWidth - j : granularity);
                    uint numPixels = widthToSum * heightToSum;
                    for (int k = 0; k < heightToSum; k++)
                    {
                        for (int l = 0; l < widthToSum; l++)
                        {
                            sumB += pixels[(i + k) * stride + (j + l) * 4];
                            sumG += pixels[(i + k) * stride + (j + l) * 4 + 1];
                            sumR += pixels[(i + k) * stride + (j + l) * 4 + 2];
                            sumA += pixels[(i + k) * stride + (j + l) * 4 + 3];
                        }
                    }
                    if (numPixels == 256)
                    {
                        sumB = sumB >> 8;
                        sumG = sumG >> 8;
                        sumR = sumR >> 8;
                        sumA = sumA >> 8;
                    }
                    else
                    {
                        sumB = sumB / numPixels;
                        sumG = sumG / numPixels;
                        sumR = sumR / numPixels;
                        sumA = sumA / numPixels;
                    }
                    for (int k = 0; k < heightToSum; k++)
                    {
                        for (int l = 0; l < widthToSum; l++)
                        {
                            pixels[(i + k) * stride + (j + l) * 4] = (byte)sumB;
                            pixels[(i + k) * stride + (j + l) * 4 + 1] = (byte)sumG;
                            pixels[(i + k) * stride + (j + l) * 4 + 2] = (byte)sumR;
                            pixels[(i + k) * stride + (j + l) * 4 + 3] = (byte)sumA;
                        }
                    }
                }
            }
            return BitmapSource.Create(original.PixelWidth, original.PixelHeight, 96, 96, PixelFormats.Bgra32, null, pixels, stride);
        }
        protected override void OnSourceInitialized(EventArgs e)
        {
            var hwndSource = (HwndSource)HwndSource.FromVisual(this);
            hwndSource.AddHook(WndHookProc);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _horizontalMargin = this.ActualWidth - ((Grid)this.Content).ActualWidth;
            _verticalMargin = this.ActualHeight - ((Grid)this.Content).ActualHeight;
            this.SizeToContent = SizeToContent.Manual;
            this.Width = Math.Max(this.ActualWidth, this.ActualHeight);
            this.Height = this.Width;
            ImageView.Width = Double.NaN;
            ImageView.Height = Double.NaN;

        }
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(this).Handle;
            PostMessage(hwnd, 0xA1, (IntPtr)2, IntPtr.Zero);
        }
        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var currentSource = this.ImageView.Source as BitmapSource;
            if (currentSource == null) return;
            if (_altSource == null)
            {
                this.ImageView.Source = ApplyMosaic(currentSource, 16);
            }
            else
            {
                this.ImageView.Source = _altSource;
            }
            _altSource = currentSource;
        }

        private const int WM_SIZING = 0x214;
        private const int WMSZ_LEFT = 1;
        private const int WMSZ_RIGHT = 2;
        private const int WMSZ_TOP = 3;
        private const int WMSZ_TOPLEFT = 4;
        private const int WMSZ_TOPRIGHT = 5;
        private const int WMSZ_BOTTOM = 6;
        private const int WMSZ_BOTTOMLEFT = 7;
        private const int WMSZ_BOTTOMRIGHT = 8;

        private IntPtr WndHookProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WM_SIZING)
            {
                var rect = (RECT)Marshal.PtrToStructure(lParam, typeof(RECT));

                var w = rect.right - rect.left - this._horizontalMargin;
                var h = rect.bottom - rect.top - this._verticalMargin;

                switch (wParam.ToInt32())
                {
                    case WMSZ_LEFT:
                    case WMSZ_RIGHT:
                        rect.bottom = (int)(rect.top + w / this._aspectRatio + this._verticalMargin);
                        break;
                    case WMSZ_TOP:
                    case WMSZ_BOTTOM:
                        rect.right = (int)(rect.left + h * this._aspectRatio + this._horizontalMargin);
                        break;
                    case WMSZ_TOPLEFT:
                        if (w / h > this._aspectRatio)
                        {
                            rect.top = (int)(rect.bottom - w / this._aspectRatio - this._verticalMargin);
                        }
                        else
                        {
                            rect.left = (int)(rect.right - h * this._aspectRatio - this._horizontalMargin);
                        }
                        break;
                    case WMSZ_TOPRIGHT:
                        if (w / h > this._aspectRatio)
                        {
                            rect.top = (int)(rect.bottom - w / this._aspectRatio - this._verticalMargin);
                        }
                        else
                        {
                            rect.right = (int)(rect.left + h * this._aspectRatio + this._horizontalMargin);
                        }
                        break;
                    case WMSZ_BOTTOMLEFT:
                        if (w / h > this._aspectRatio)
                        {
                            rect.bottom = (int)(rect.top + w / this._aspectRatio + this._verticalMargin);
                        }
                        else
                        {
                            rect.left = (int)(rect.right - h * this._aspectRatio - this._horizontalMargin);
                        }
                        break;
                    case WMSZ_BOTTOMRIGHT:
                        if (w / h > this._aspectRatio)
                        {
                            rect.bottom = (int)(rect.top + w / this._aspectRatio + this._verticalMargin);
                        }
                        else
                        {
                            rect.right = (int)(rect.left + h * this._aspectRatio + this._horizontalMargin);
                        }
                        break;
                    default:
                        break;
                }
                Marshal.StructureToPtr(rect, lParam, true);
            }
            return IntPtr.Zero;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        private void OpenImageWithWebBrowser(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(OriginalMediaURL))
            {
                try
                {
                    System.Diagnostics.Process.Start(OriginalMediaURL);
                }
                catch (Exception)
                {

                }
            }
        }
    }
}
