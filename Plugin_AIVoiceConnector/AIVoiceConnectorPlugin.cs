﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using FNF.BouyomiChanApp;
using FNF.JsonParser;
using FNF.Utility;
using FNF.XmlSerializerSetting;

namespace Plugin_AIVoiceConnector
{
    public class AIVoiceConnectorPlugin : IPlugin
    {
        public string Name => "AIVoiceConnector";
        public string Version => "2024/08/18版";
        public string Caption => "A.I.Voiceを使って読み上げを行います";
        public ISettingFormData SettingFormData => null;
        public string PipeNamePrefix => "AIVoiceConnector";
        private string _pipeName;
        private Process _helperProcess;
        public void Begin()
        {
            string pipeSuffix = $"{DateTime.Now.Ticks}";
            _pipeName = $"{PipeNamePrefix}-{pipeSuffix}";
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.FileName = "Plugin_AIVoiceConnector.Helper.exe";
            psi.Arguments = pipeSuffix;
            try
            {
                _helperProcess = Process.Start(psi);
            }
            catch (Exception)
            {
                MessageBox.Show("Plugin_AIVoiceConnector.Helper.exe を起動できません。ファイルが正しく配置されているか確認してください。", "Plugin_AIVoiceConnector");
                return;
            }
            for (int i=20001; i<=20020; i++)
            {
                Pub.FormMain.BC.Voices.Add((VoiceType)i, Pub.FormMain.BC.Voices[(VoiceType)1]);
            }
            Pub.FormMain.BC.TalkTaskStarted += new EventHandler<BouyomiChan.TalkTaskStartedEventArgs>(TalkTaskStarted);
        }
        public void End()
        {
            if (_helperProcess == null) return;
            Pub.FormMain.BC.TalkTaskStarted -= new EventHandler<BouyomiChan.TalkTaskStartedEventArgs>(TalkTaskStarted);
            for (int i = 20001; i <= 20020; i++)
            {
                Pub.FormMain.BC.Voices.Remove((VoiceType)i);
            }
            if (!_helperProcess.HasExited) _helperProcess.Kill();
        }
        private void TalkTaskStarted(object sender, BouyomiChan.TalkTaskStartedEventArgs e)
        {
            bool useDefaultVoice = false;
            while (true)
            {
                if (_helperProcess == null || _helperProcess.HasExited)
                {
                    useDefaultVoice = true;
                    break;
                }
                int voiceId = (int)e.TalkTask.VoiceType;
                if (voiceId != 0 && voiceId < 20001)
                {
                    useDefaultVoice = true;
                    break;
                }

                if (e.ReplaceWord.Contains("(Ｔ "))
                {
                    useDefaultVoice = true;
                    break;
                }
                var text = e.ReplaceWord;
                var volume = e.TalkTask.Volume;
                var speed = e.TalkTask.Speed;
                var pitch = e.TalkTask.Tone;
                try
                {
                    Type type = Pub.FormMain.BC.GetType();
                    FieldInfo taskCancelledField = type.GetField("_TalkTaskCanceled", BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance);
                    FieldInfo taskSkippedField = type.GetField("_SkipTalkTaskFlag", BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance);
                    var jsonText = $"{{\"command\":\"play\", \"text\":\"{EscapeForJson(text)}\"";
                    if (volume >= 0 || speed >= 0 || pitch >= 0 || voiceId >= 20001)
                    {
                        bool added = false;
                        jsonText += ", \"params\":{";
                        if (volume >= 0)
                        {
                            added = true;
                            jsonText += $"\"volume\":{volume}";
                        }
                        if (speed >= 0)
                        {
                            if (added) jsonText += ", ";
                            else added = true;
                            jsonText += $"\"speed\":{speed}";
                        }
                        if (pitch >= 0)
                        {
                            if (added) jsonText += ", ";
                            else added = true;
                            jsonText += $"\"pitch\":{pitch}";
                        }
                        if (voiceId >= 20001)
                        {
                            if (added) jsonText += ", ";
                            jsonText += $"\"presetIndex\":{voiceId - 20000}";
                        }
                        jsonText += "}";
                    }
                    jsonText += "}";
                    using (var pipe = new NamedPipeClientStream(".", _pipeName, PipeDirection.InOut))
                    {
                        pipe.Connect(5000);
                        using (var writer = new StreamWriter(pipe))
                        {
                            writer.WriteLine(jsonText);
                            writer.Flush();
                            var reader = new StreamReader(pipe);
                            var ret = reader.ReadLine();
                            var json = new Parser(ret.Trim());
                            if (json["status"].String != "ok")
                            {
                                useDefaultVoice = true;
                                break;
                            }
                            bool cancelled = false;
                            do
                            {
                                if (!cancelled && ((bool)(taskCancelledField.GetValue(Pub.FormMain.BC)) || (bool)(taskSkippedField.GetValue(Pub.FormMain.BC))))
                                {
                                    writer.WriteLine("{command:\"skip\"}");
                                    writer.Flush();
                                    cancelled = true;
                                    reader.ReadLine();
                                    continue;
                                }
                                Thread.Sleep(100);
                                writer.WriteLine("{command:\"getStatus\"}");
                                writer.Flush();
                                ret = reader.ReadLine();
                                json = new Parser(ret.Trim());
                                if (json["status"].String != "busy")
                                {
                                    break;
                                }
                            } while (true);
                            if (json["status"].String != "done")
                            {
                                useDefaultVoice = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    useDefaultVoice = true;
                }
                break;
            }
            e.Cancel = !useDefaultVoice;
        }
        public static string EscapeForJson(string s)
        {
            if (s == null || s.Length == 0)
            {
                return "";
            }

            int len = s.Length;
            var sb = new StringBuilder(len + 4);
            for (int i = 0; i < len; i += 1)
            {
                char c = s[i];
                switch (c)
                {
                    case '\\':
                    case '"':
                        sb.Append('\\');
                        sb.Append(c);
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    default:
                        if (c < ' ')
                        {
                            string t = "000" + String.Format("X", c);
                            sb.Append("\\u" + t.Substring(t.Length - 4));
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            return sb.ToString();
        }
    }
}
